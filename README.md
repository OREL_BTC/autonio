# [Autonio](https://www.autonio.foundation) - an open-source decentralized cryptocurrency trading terminal


AutoNIO Trading Terminal is a decentralized application that opens access to algorithmic trading techniques on the market's top cryptocurrency exchanges. Build, sell, or buy intelligent trading algorithms with a user-friendly interface.


![Backtest](https://gitlab.com/autonio/autonio/uploads/2cae59439045e8f7a506a41b9408bb23/backtest.gif)

![Livetrade](https://gitlab.com/autonio/autonio/uploads/b47d97954204afe8ec4f8528e1678823/livetrade.gif)

![Algorithm Marketplace](https://gitlab.com/autonio/autonio/uploads/1df69b4d35288b5e405404575dccb72b/algo-mkt-place.gif)

![Buy Algo](https://gitlab.com/autonio/autonio/uploads/f31cf3af0943ab64a90cc3bbe1038e23/buy-algo.gif)


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Licensing

Autonio is licensed under the [Apache License](LICENSE), Version 2.0.


# Autonio
