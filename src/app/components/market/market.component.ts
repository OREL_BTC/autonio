import { Component, OnInit } from '@angular/core';
import { WebService } from './../../services/web.service';
import { Http } from '@angular/http';
import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NgStyle } from '@angular/common';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit {

  algos = [];
  asset;
  base;
  exc;

  ti = ti1['window'];

  maxDate = new Date();
  minDate;

  chartData;
  settings;
  final_buy_sell = [];
  chart_config = '';
  portfolio;
  exchange_fees;
  signal_inverse;
  trade_count = 0;

  chart;

  seller_amount;
  seller_address;
  s_id;

  modal_open_backtest: boolean = false;
  modal_open_buy: boolean = false;
  blurCss = { 'filter': 'blur(0px)' };

  autonio_token;

  constructor(private AmCharts: AmChartsService, public router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }


    this.checkBlur();
    var today = new Date();
    var newdate = new Date();
    newdate.setDate(today.getDate() - 1);
    this.minDate = newdate;

    $.ajax({
      url: 'https://autonio.foundation/webservices/get_sell_settings.php',
      method: 'POST',
      success: (algos) => {
        if (algos == 200 && algos == 404) {

        }
        else {
          this.algos = algos;
        }
      }
    });
  }

  electron_er(msg) {
    alert(msg);
  }

  PerCalc(a, b) {
    return parseFloat(((a / 100) * b).toFixed(8));
  }

  ModalClose() {
    this.modal_open_backtest = false;
    this.modal_open_buy = false;
    this.checkBlur();
  }

  checkBlur() {
    let style;
    if (this.modal_open_backtest || this.modal_open_buy) {
      this.blurCss = { 'filter': 'blur(5px)' };
    }
    else {
      this.blurCss = { 'filter': 'blur(0px)' };
    }
    return style;
  }

  PairChange(exc, pair) {
    (async () => {
      let val1 = new ccxt[exc];
      await val1.loadMarkets();
      let pair1 = val1.markets[pair];
      this.asset = pair1['base'];
      this.base = pair1['quote'];
    })()
  }

  IndIntChange(int) {
    let exc = this.exc;
    let a, b;
    if (this.asset == 'USDT') {
      a = 'USD';
    }
    else {
      a = this.asset;
    }
    if (this.base == 'USDT') {
      b = 'USD';
    }
    else {
      b = this.base;
    }

    if (int < 60) {
      let options = { aggregate: int, exchange: exc, limit: 20000000 };
      cc.histoMinute(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
    else if (int >= 60 && int < 1440) {
      let options = { aggregate: int / 60, exchange: exc, limit: 20000000 };
      cc.histoHour(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
    else {
      let options = { aggregate: int / 1440, exchange: exc, limit: 20000000 };
      cc.histoDay(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
  }

  InverseToggle() {
    this.signal_inverse = !this.signal_inverse;
  }

  formatDate(date) {
    return mnt(date).format('YYYY-MM-DD HH:mm');
  }

  BuySettingsModal(id, seller, price_usd) {
    this.modal_open_buy = true;
    this.checkBlur();
    this.seller_amount = 0;
    this.seller_address = seller;
    this.s_id = id;
    $.ajax({
      url: 'https://min-api.cryptocompare.com/data/histoday?fsym=NIO&tsym=USD&limit=2&aggregate=1&e=CCCAGG',
      datatype: 'json',
      success: (data) => {
        let Data = data['Data'];
        let close = Data[Data.length - 2]['close'];
        this.seller_amount = Math.round((price_usd / close));
      }
    });
  }

  CheckPayment() {
    let gmt;
    $.ajax({
      url: 'http://api.timezonedb.com/v2/get-time-zone?key=WM7ACYIY2M17&format=json&by=zone&zone=Europe/London',
      success: (data) => {
        gmt = data['timestamp'];
        let gmt_date:any = new Date(gmt * 1000);

        $.ajax({
          url: 'https://api.ethplorer.io/getAddressHistory/' + this.seller_address + '?apiKey=freekey&type=transfer',
          success: (data) => {
            data = JSON.parse(data).operations;
            let compare_date;
            let error = 1;
            let check_amount = this.seller_amount * 0.8;
            let received_amnt;
            $.each(data, (i, val) => {
              compare_date = new Date(val.timestamp * 1000);
              var diffMs = gmt_date - compare_date;
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
              received_amnt = parseInt(val.value);
              if (diffMins <= 30 && val.value >= check_amount && val.from == this.autonio_token.username) {
                error = 0;
              }
            });

            if (error == 0) {
              let buy_data = {
                's_id': this.s_id,
                'username': this.autonio_token.username,
                'amount': received_amnt
              };
              $.ajax({
                url: 'https://autonio.foundation/webservices/user_buy.php',
                method: 'POST',
                data: buy_data,
                success: (data1) => {
                  data1 = parseInt(data1);
                  switch (data1) {
                    case 400:
                      this.electron_er('Success, Now you can see bought settings in "BUYING" Section.');
                      this.ModalClose();
                      break;

                    case 404:
                      this.electron_er('Some Error Occured, Please try again later');
                      this.ModalClose();
                      break;
                  }
                }
              });
            }
            else {
              this.electron_er('There is some error, Possibly you sent wrong amount or you sent from another address than your registered address.');
            }
          }
        });
      }
    });
    // https://api.ethplorer.io/getAddressHistory/0xff44d051383b08e1f56fcbc0cf1e4684bc6d2196?apiKey=freekey&token=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&type=transfer

  }

  RunBacktest(exc, pair, settings) {
    this.modal_open_backtest = true;
    this.checkBlur();
    settings = JSON.parse(settings);
    this.settings = settings;
    this.PairChange(exc, pair);
    this.exc = exc;
  }

  Backtest() {
    var interval, after, before, period, exchange;
    $(document).ready(() => {
      function toTimestamp(strDate) {
        var datum = Date.parse(strDate);
        return datum / 1000;
      }
      let asset: number = parseFloat($('.modal input[name="asset"]').val());
      let start_asset: number = asset;
      let base: number = parseFloat($('.modal input[name="base"]').val());
      var portfolio_perc = parseFloat($('.modal input[name="portfolio"]').val());
      let start_base: number = base;
      var startdate = $('.modal input[name="startdate"]').val();
      var enddate = $('.modal input[name="enddate"]').val();
      interval = $('.modal select[name="ind_int"]').val();
      exchange = this.exc;
      if (interval == '' || startdate == '' || enddate == '') {
        this.electron_er('Some required fields are missing like dates or interval');
      }
      else {
        this.chartData = '';
        this.final_buy_sell = [];
        this.chart_config = '';
        this.portfolio = parseFloat(this.PerCalc(start_base, portfolio_perc).toFixed(8));
        this.exchange_fees = parseFloat($('input[name="fees"]').val());
        this.trade_count = 0;
        after = toTimestamp(startdate);
        before = toTimestamp(enddate);
        period = interval;
        this.GetBuySell(interval, exchange, after, before, period, asset, base, start_asset, start_base);
      }
    });
  }

  GetBuySell(interval, exchange, after, before, period, asset, base, start_asset, start_base) {
    $(() => {
      var items = [];
      let final_data = this.settings;

      if (final_data.length > 0) {
        var timestamp = [];
        var op = [];
        var hp = [];
        var lp = [];
        var cp = [];
        var volume = [];
        let a, b;
        if (this.asset == 'USDT') {
          a = 'USD';
        }
        else {
          a = this.asset;
        }
        if (this.base == 'USDT') {
          b = 'USD';
        }
        else {
          b = this.base;
        }
        if (period < 60) {
          let options = { aggregate: period, timestamp: new Date(before * 1000), exchange: exchange, limit: 20000000 };
          cc.histoMinute(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });
              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base);
            })
            .catch();
        }
        else if (period >= 60 && period < 1440) {
          let options = { aggregate: period / 60, timestamp: new Date(before * 1000), exchange: exchange, limit: 20000000 };
          cc.histoHour(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base);
            })
            .catch();
        }
        else {
          let options = { aggregate: period / 1440, timestamp: new Date(before * 1000), exchange: exchange, limit: 20000000 };
          cc.histoDay(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base);
            })
            .catch();
        }
      }
      else {
        this.electron_er('Please select at least one indicator');
      }
    });
  }
  PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base) {
    $('.container .actual_chart .chart_preloader').fadeIn(200);
    $('.result_preloader').css('display', 'flex');
    $('.result_preloader img').fadeIn(200);
    $('.container .container-body .result .section img').fadeOut(100);
    var indi_ar = [];
    var indi_name_ar = [];
    var indi_params = [];
    function array_pos_1(start, ar1, ar2) {
      var ar_return = [];
      for (var i = start, j = 0; i < ar2.length; i++ , j++) {
        if (j < start) {
          ar_return[j] = '';
        }
        ar_return[i] = ar1[j];
      }
      return ar_return;
    }
    var buy, sell;
    var input, tp;
    $.each(final_data, (i, val) => {
      var indi = (Object.keys(val)[0]);
      $.each(val, function (j, val2) {
        indi_params = val2;
      });
      switch (indi) {
        case 'adl':
          input = { high: hp, low: lp, close: cp, volume: volume };
          var adl_res = this.ti.adl(input);
          var adl_final = [];
          buy = 0;
          sell = 0;
          $.each(adl_res, function (i, val) {
            if (val >= 0 && buy == 0) {
              adl_final[i] = 'BUY';
              buy = 1;
              sell = 0;
            }
            else if (val < 0 && sell == 0) {
              adl_final[i] = 'SELL';
              buy = 0;
              sell = 1;
            }
            else {
              adl_final[i] = '';
            }
          });
          indi_ar.push(adl_final);
          break;
        case 'adx':
          tp = indi_params[0]['adx_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var adx_final = [];
          var adx_res_temp = this.ti.adx(input);
          var adx_res_temp2 = [];
          buy = 0;
          sell = 0;
          $.each(adx_res_temp, function (i, val) {
            if (val['mdi'] >= val['pdi'] && buy == 0) {
              adx_res_temp2.push('BUY');
              buy = 1;
              sell = 0;
            }
            else if (val['mdi'] < val['pdi'] && sell == 0) {
              adx_res_temp2.push('SELL');
              buy = 0;
              sell = 1;
            }
            else {
              adx_res_temp2.push('');
            }
          });
          var starting_index = cp.length - adx_res_temp2.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              adx_final[j] = '';
            }
            adx_final[i] = adx_res_temp2[j];
          }      
          indi_ar.push(adx_final);
          break;

        case 'atr':
          tp = indi_params[0]['atr_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var atr_res_temp = this.ti.atr(input);
          var atr_res = [];
          var atr_final = [];
          starting_index = cp.length - atr_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              atr_res[j] = '';
            }
            atr_res[i] = atr_res_temp[j];
          }
          buy = 0;
          sell = 0;
          $.each(atr_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                atr_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                atr_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                atr_final[i] = '';
              }
            }
            else {
              atr_final[i] = '';
            }
          });
          // console.log(atr_final);
          indi_ar.push(atr_final);
          break;

        case 'bollingerbands':
          tp = parseInt(indi_params[0]['bb_time_period']);
          var sd1 = parseInt(indi_params[1]['bb_stddev']);
          input = { period: tp, values: cp, stdDev: sd1 };
          // console.log(input);
          var bollingerbands_res = this.ti.bollingerbands(input);
          var bb_lower = [];
          var bb_upper = [];
          var bollingerbands_final = [];
          var remain = cp.length - bollingerbands_res.length;
          j = remain;
          $.each(bollingerbands_res, function (i, val) {
            if (i < remain) {
              bb_lower[i] = '';
              bb_upper[i] = '';
            }
            bb_lower[j] = val['lower'];
            bb_upper[j] = val['upper'];
            j++;
          });
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (val != '') {
              if (val > bb_upper[i] && buy == 0) {
                bollingerbands_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val < bb_lower[i] && sell == 0) {
                bollingerbands_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                bollingerbands_final[i] = '';
              }
            }
            else {
              bollingerbands_final[i] = '';
            }
          });
          indi_ar.push(bollingerbands_final);
          // console.log(bollingerbands_final);
          break;

        case 'cci':
          tp = parseInt(indi_params[0]['cci_time_period']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp };
          var cci_res_temp = this.ti.cci(input);
          var cci_res = [];
          var cci_final = [];
          starting_index = cp.length - cci_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              cci_res[j] = '';
            }
            cci_res[i] = cci_res_temp[j];
          }
          sell = 0;
          buy = 0;
          $.each(cci_res, function (i, val) {
            if (val != '') {
              if (val >= 100 && buy == 0) {
                cci_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val < (-100) && sell == 0) {
                cci_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                cci_final[i] = '';
              }
            }
            else {
              cci_final[i] = '';
            }
          });
          indi_ar.push(cci_final);
          // console.log(cci_final);
          break;

        case 'forceindex':
          tp = parseInt(indi_params[0]['fi_time_period']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp, volume: volume };
          var forceindex_res_temp = this.ti.forceindex(input);
          var forceindex_final = [];
          starting_index = cp.length - forceindex_res_temp.length;
          var forceindex_res = array_pos_1(starting_index, forceindex_res_temp, cp);
          buy = 0;
          sell = 0;
          $.each(forceindex_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                forceindex_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                forceindex_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                forceindex_final[i] = '';
              }
            }
            else {
              forceindex_final[i] = '';
            }
          });
          indi_ar.push(forceindex_final);
          // console.log(forceindex_final);
          break;

        case 'kst':
          tp = parseInt(indi_params[0]['kst_time_period']);
          var roc1 = parseInt(indi_params[1]['kst_roc1']);
          var roc2 = parseInt(indi_params[2]['kst_roc2']);
          var roc3 = parseInt(indi_params[3]['kst_roc3']);
          var roc4 = parseInt(indi_params[4]['kst_roc4']);
          var smroc1 = parseInt(indi_params[5]['kst_smroc1']);
          var smroc2 = parseInt(indi_params[6]['kst_smroc2']);
          var smroc3 = parseInt(indi_params[7]['kst_smroc3']);
          var smroc4 = parseInt(indi_params[8]['kst_smroc4']);
          input = {
            values: cp,
            ROCPer1: roc1,
            ROCPer2: roc2,
            ROCPer3: roc3,
            ROCPer4: roc4,
            SMAROCPer1: smroc1,
            SMAROCPer2: smroc2,
            SMAROCPer3: smroc3,
            SMAROCPer4: smroc4,
            signalPeriod: tp
          };
          var kst_res_temp = this.ti.kst(input);
          var kst_res_temp1 = [];
          $.each(kst_res_temp, function (i, val) {
            kst_res_temp1.push(val['kst']);
          });
          starting_index = cp.length - kst_res_temp1.length;
          var kst_res = array_pos_1(starting_index, kst_res_temp1, cp);
          var kst_final = [];
          buy = 0;
          sell = 0;
          $.each(kst_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                kst_final[i] = 'BUY'
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                kst_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                kst_final[i] = '';
              }
            }
            else {
              kst_final[i] = '';
            }
          });
          indi_ar.push(kst_final);
          // console.log(kst_final);
          break;

        case 'macd':
          var fp = parseInt(indi_params[0]['macd_fast_period']);
          var sp = parseInt(indi_params[1]['macd_slow_period']);
          var slp = parseInt(indi_params[2]['macd_signal_period']);
          input = {
            values: cp,
            fastPeriod: fp,
            slowPeriod: sp,
            signalPeriod: slp,
            SimpleMAOscillator: false,
            SimpleMASignal: false
          };
          var macd_res_temp = this.ti.macd(input);
          var macd_res_temp1 = [];
          var macd_final = [];
          $.each(macd_res_temp, function (i, val) {
            if (val['signal'] != undefined) {
              macd_res_temp1[i] = val['signal'];
            }
            else {
              macd_res_temp1[i] = '';
            }
          });
          starting_index = cp.length - macd_res_temp1.length;
          var macd_res = array_pos_1(starting_index, macd_res_temp1, cp);
          buy = 0;
          sell = 0;
          $.each(macd_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                macd_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                macd_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                macd_final[i] = '';
              }
            }
            else {
              macd_final[i] = '';
            }
          });
          indi_ar.push(macd_final);
          // console.log(macd_final);
          break;

        case 'obv':
          input = { close: cp, volume: volume };
          var obv_res_temp = this.ti.obv(input);
          starting_index = cp.length - obv_res_temp.length;
          var obv_res = array_pos_1(starting_index, obv_res_temp, cp);
          buy = 0;
          sell = 0;
          var obv_final = [];
          $.each(obv_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                obv_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                obv_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                obv_final[i] = '';
              }
            }
            else {
              obv_final[i] = '';
            }
          });
          indi_ar.push(obv_final);
          // console.log(obv_final);
          break;

        case 'psar':
          var step = parseInt(indi_params[0]['psar_steps']);
          var max = parseInt(indi_params[1]['psar_max']);
          input = { high: hp, low: lp, step: step, max: max };
          var psar_res_temp = this.ti.psar(input);
          var psar_final = [];
          buy = 0;
          sell = 0;
          $.each(psar_res_temp, function (i, val) {
            if (cp[i] >= val && buy == 0) {
              psar_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (cp[i] < val && sell == 0) {
              psar_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              psar_final[i] = '';
            }
          });
          indi_ar.push(psar_final);
          // console.log(psar_final);
          break;

        case 'roc':
          tp = parseInt(indi_params[0]['roc_time_period']);
          input = { period: tp, values: cp };
          var roc_res_temp = this.ti.roc(input);
          starting_index = cp.length - roc_res_temp.length;
          var roc_res = array_pos_1(starting_index, roc_res_temp, cp);
          var roc_final = [];
          buy = 0;
          sell = 0;
          $.each(roc_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                roc_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                roc_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                roc_final[i] = '';
              }
            }
            else {
              roc_final[i] = '';
            }
          });
          indi_ar.push(roc_final);
          // console.log(roc_final);
          break;

        case 'rsi':
          tp = parseInt(indi_params[0]['rsi_time_period']);
          input = { period: tp, values: cp };
          var rsi_res_temp = this.ti.rsi(input);
          starting_index = cp.length - rsi_res_temp.length;
          var rsi_res = array_pos_1(starting_index, rsi_res_temp, cp);
          var rsi_final = [];
          buy = 0;
          sell = 0;
          $.each(rsi_res, function (i, val) {
            if (val != '') {
              if (val <= 30 && buy == 0) {
                rsi_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val >= 70 && sell == 0) {
                rsi_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                rsi_final[i] = '';
              }
            }
            else {
              rsi_final[i] = '';
            }
          });
          indi_ar.push(rsi_final);
          // console.log(rsi_final);
          break;

        case 'sma':
          var short = parseInt(indi_params[0]['sma_short_period']);
          var input_short = { period: short, values: cp };
          var long = parseInt(indi_params[1]['sma_long_period']);
          var input_long = { period: long, values: cp };
          var sma_short_temp = this.ti.sma(input_short);
          var sma_long_temp = this.ti.sma(input_long);
          var starting_index_short = cp.length - sma_short_temp.length;
          var sma_short_temp1 = array_pos_1(starting_index_short, sma_short_temp, cp);
          var starting_index_long = cp.length - sma_long_temp.length;
          var sma_long_temp1 = array_pos_1(starting_index_long, sma_long_temp, cp);
          var sma_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (sma_short_temp1[i] != '' && sma_long_temp1[i] != '') {
              if (sma_short_temp1[i] >= sma_long_temp1[i] && buy == 0) {
                sma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (sma_short_temp1[i] < sma_long_temp1[i] && sell == 0) {
                sma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                sma_final[i] = '';
              }
            }
            else {
              sma_final[i] = '';
            }
          });
          indi_ar.push(sma_final);
          // console.log(sma_final);
          break;

        case 'ema':
          short = parseInt(indi_params[0]['ema_short_period']);
          input_short = { period: short, values: cp };
          long = parseInt(indi_params[1]['ema_long_period']);
          input_long = { period: long, values: cp };
          var ema_short_temp = this.ti.ema(input_short);
          var ema_long_temp = this.ti.ema(input_long);
          starting_index_short = cp.length - ema_short_temp.length;
          var ema_short_temp1 = array_pos_1(starting_index_short, ema_short_temp, cp);
          starting_index_long = cp.length - ema_long_temp.length;
          var ema_long_temp1 = array_pos_1(starting_index_long, ema_long_temp, cp);
          var ema_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (ema_short_temp1[i] != '' && ema_long_temp1[i] != '') {
              if (ema_short_temp1[i] >= ema_long_temp1[i] && buy == 0) {
                ema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (ema_short_temp1[i] < ema_long_temp1[i] && sell == 0) {
                ema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                ema_final[i] = '';
              }
            }
            else {
              ema_final[i] = '';
            }
          });
          indi_ar.push(ema_final);
          // console.log(ema_final);
          break;

        case 'stochastic':
          tp = parseInt(indi_params[0]['kd_time_period']);
          sp = parseInt(indi_params[1]['kd_signal_period']);
          input = { high: hp, low: lp, close: cp, period: tp, signalPeriod: sp };
          var stochastic_res_temp = this.ti.stochastic(input);
          var stochastic_final = [];
          var stochastic_k = [];
          var stochastic_d = [];
          remain = cp.length - stochastic_res_temp.length;
          j = remain;
          buy = 0;
          sell = 0;
          $.each(stochastic_res_temp, function (i, val) {
            if (val['k'] != undefined && val['d'] != undefined) {
              stochastic_k.push(val['k']);
              stochastic_d.push(val['d']);
            }
            else {
              stochastic_k.push('');
              stochastic_d.push('');
            }
          });
          var starting_index_k = cp.length - stochastic_k.length;
          var stochastic_k1 = array_pos_1(starting_index_k, stochastic_k, cp);
          var starting_index_d = cp.length - stochastic_d.length;
          var stochastic_d1 = array_pos_1(starting_index_d, stochastic_d, cp);
          $.each(cp, function (i, val) {
            if (stochastic_k1[i] != '' && stochastic_d1[i] != '') {
              if (stochastic_d1[i] >= stochastic_k1[i] && buy == 0) {
                stochastic_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (stochastic_d1[i] < stochastic_k1[i] && sell == 0) {
                stochastic_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                stochastic_final[i] = '';
              }
            }
            else {
              stochastic_final[i] = '';
            }
          });
          indi_ar.push(stochastic_final);
          // console.log(stochastic_final);
          break;

        case 'trix':
          tp = parseInt(indi_params[0]['trix_time_period']);
          input = { values: cp, period: tp };
          var trix_res_temp = this.ti.trix(input);
          starting_index = cp.length - trix_res_temp.length;
          var trix_res = array_pos_1(starting_index, trix_res_temp, cp);
          var trix_final = [];
          buy = 0; sell = 0;
          $.each(trix_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                trix_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                trix_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                trix_final[i] = '';
              }
            }
            else {
              trix_final[i] = '';
            }
          });
          indi_ar.push(trix_final);
          // console.log(trix_final);
          break;

        case 'vwap':
          input = {
            open: op,
            high: hp,
            low: lp,
            close: cp,
            volume: volume
          };
          var vwap_res_temp = this.ti.vwap(input);
          var vwap_final = [];
          buy = 0; sell = 0;
          $.each(vwap_res_temp, function (i, val) {
            if (val >= cp[i] && buy == 0) {
              vwap_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (val < cp[i] && sell == 0) {
              vwap_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              vwap_final[i] = '';
            }
          });
          indi_ar.push(vwap_final);
          // console.log(vwap_final);
          break;

        case 'wma':
          tp = parseInt(indi_params[0]['wma_time_period']);
          input = { values: cp, period: tp };
          var wma_res_temp = this.ti.wma(input);
          starting_index = cp.length - wma_res_temp.length;
          var wma_res = array_pos_1(starting_index, wma_res_temp, cp);
          var wma_final = [];
          buy = 0; sell = 0;
          $.each(wma_res, function (i, val) {
            if (val != '') {
              if (val >= cp[i] && buy == 0) {
                wma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < cp[i] && sell == 0) {
                wma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wma_final[i] = '';
              }
            }
            else {
              wma_final[i] = '';
            }
          });
          indi_ar.push(wma_final);
          // console.log(wma_final);
          break;

        case 'wema':
          tp = parseInt(indi_params[0]['wema_time_period']);
          input = { values: cp, period: tp };
          var wema_res_temp = this.ti.wema(input);
          starting_index = cp.length - wema_res_temp.length;
          var wema_res = array_pos_1(starting_index, wema_res_temp, cp);
          var wema_final = [];
          buy = 0; sell = 0;
          $.each(wema_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                wema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                wema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wema_final[i] = '';
              }
            }
            else {
              wema_final[i] = '';
            }
          });
          indi_ar.push(wema_final);
          // console.log(wema_final);
          break;

        case 'williamsr':
          tp = parseInt(indi_params[0]['williamsr_time_period']);
          input = { high: hp, low: lp, close: cp, period: tp };
          var williamsr_res_temp = this.ti.williamsr(input);
          starting_index = cp.length - williamsr_res_temp.length;
          var williamsr_res = array_pos_1(starting_index, williamsr_res_temp, cp);
          var williamsr_final = [];
          buy = 0; sell = 0;
          $.each(williamsr_res, function (i, val) {
            if (val != '') {
              if (val >= 20 && buy == 0) {
                williamsr_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 20 && sell == 0) {
                williamsr_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                williamsr_final[i] = '';
              }
            }
            else {
              williamsr_final[i] = '';
            }
          });
          indi_ar.push(williamsr_final);
          // console.log(williamsr_final);
          break;

        default:
          // console.log('Rest');
          break;
      }
      indi_name_ar.push(indi + '_final');
    });

    // console.log(indi_ar);
    var hold_key_ar = [];
    var buy_key_ar = [];
    var sell_key_ar = [];
    $.each(cp, function (i, val) {
      buy_key_ar[i] = 0;
      sell_key_ar[i] = 0;
      hold_key_ar[i] = 0;
    });
    $.each(indi_ar, function (i, val) {
      $.each(val, function (j, val1) {
        if (val1 == 'SELL') {
          sell_key_ar[j] += 1;
        }
        else if (val1 == 'BUY') {
          buy_key_ar[j] += 1;
        }
        else {
          hold_key_ar[j] += 1;
        }
      });
    });
    buy = 0; sell = 0;

    let buy_sell = [];
    buy = 0; sell = 0;

    $.each(cp, (i, val) => {
      if (sell_key_ar[i] > buy_key_ar[i] && sell == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'BUY';
        }
        else {
          buy_sell[i] = 'SELL';
        }
      }
      else if (sell_key_ar[i] < buy_key_ar[i] && buy == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'SELL';
        }
        else {
          buy_sell[i] = 'BUY';
        }
      }
      else {
        buy_sell[i] = '';
      }
    });
    let start_bal = ((asset * cp[0]) + base).toFixed(8);

    $.each(buy_sell, (i, val) => {
      if (val == 'SELL' && sell == 0) {
        this.final_buy_sell[i] = 'SELL';
        base += parseFloat((cp[i] * asset).toFixed(8));
        asset = 0;
        sell = 1; buy = 0;
      }
      else if (val == 'BUY' && buy == 0) {
        this.final_buy_sell[i] = 'BUY';
        asset += parseFloat((this.portfolio / cp[i]).toFixed(8));
        base -= this.portfolio;
        sell = 0; buy = 1;
      }
      else {
        this.final_buy_sell[i] = '';
      }
    });

    this.MakeChart(timestamp, cp, start_bal, asset, base, start_asset, start_base);
  }

  MakeChart(timestamp, cp, start_bal, asset, base, start_asset, start_base) {
    asset = parseFloat(asset);
    base = parseFloat(base);
    start_asset = parseFloat(start_asset);
    start_base = parseFloat(start_base);

    let chartData = '[';
    $.each(cp, (i, val) => {
      let timeStamp = new Date(timestamp[i] * 1000);
      let formated_date = this.formatDate(timeStamp.toString());
      if (this.final_buy_sell[i] == 'BUY') {
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + ', "customBullet" : "assets/svg/buy.svg"}';
        this.trade_count++;
      }
      else if (this.final_buy_sell[i] == 'SELL') {
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + ', "customBullet" : "assets/svg/sell.svg"}';
        this.trade_count++;
      }
      else {
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + '}';
      }
      if (i < this.final_buy_sell.length - 1) {
        chartData += ',';
      }
    });
    chartData += ']';
    this.chartData = (JSON.parse(chartData));

    let end_bal: number = parseFloat(((asset * cp[cp.length - 1]) + base).toFixed(8));
    end_bal = parseFloat((end_bal - ((this.trade_count) * (this.portfolio) * (this.exchange_fees / 100))).toFixed(8));

    let profit_loss: any = (end_bal - start_bal).toFixed(8);
    let buy_hold: any = (start_asset * cp[cp.length - 1]) + start_base;
    buy_hold = buy_hold.toFixed(8);
    let profit_loss_buy_hold: any = (end_bal - buy_hold).toFixed(8);

    var base_c = this.base.toUpperCase();

    $('#chart').css('height', '250px').css('margin-bottom', '15px');
    $('.result').css('display', 'flex');
    $('.result_preloader').fadeIn(200);

    let chart_config = {
      "type": "serial",
      "theme": "dark",
      "addClassNames": true,
      "dataProvider": this.chartData,
      "dataDateFormat": "YYYY-MM-DD HH:NN",
      "categoryField": "date",
      "creditsPosition": "bottom-right",
      "valueAxes": [{
        "axisAlpha": 0.2,
        "dashLength": 1,
        "position": "left"
      }],
      "defs": {
        "filter": {
          "id": "dropshadow",
          "x": "-10%",
          "y": "-10%",
          "width": "120%",
          "height": "120%",
          "feOffset": {
            "result": "offOut",
            "in": "rgba",
            "dx": "3",
            "dy": "3"
          },
          "feGaussianBlur": {
            "result": "blurOut",
            "in": "offOut",
            "stdDeviation": "5"
          },
          "feBlend": {
            "in": "SourceGraphic",
            "in2": "blurOut",
            "mode": "normal"
          }
        }
      },
      "graphs": [{
        "id": "g1",
        "type": "smoothedLine",
        "bulletSize": 28,
        // "customBullet": '',
        "customBulletField": "customBullet",
        "balloonText": "[[value]]",
        "lineColor": "var(--blue-2)",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 0,
        "valueField": "value",
        "useLineColorForBulletBorder": true,
        "lineThickness": 2
        // "balloon":{
        //     "drop":true
        // }
      }],
      "chartCursor": {
        "categoryBalloonDateFormat": "HH:NN, DD MMMM",
        "cursorPosition": "mouse"
      },
      "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
      },
      "categoryAxis": {
        "minPeriod": "mm",
        "parseDates": true,
        "axisColor": "#DADADA",
        "dashLength": 1,
        "minorGridEnabled": true
      }
    };

    this.chart = this.AmCharts.makeChart("chart", chart_config);

    $('.chart_preloader').fadeOut(200);

    $('#start_bal .amount').html(start_bal + ' ' + base_c);
    $('#end_bal .amount').html(end_bal + ' ' + base_c);
    if (profit_loss < 0) {
      $('#profit_loss .amount').removeClass('green').addClass('red');
      // $('#profit_loss img').attr('src', './assets/svg/sell.svg');
    }
    else {
      $('#profit_loss .amount').removeClass('red').addClass('green');
      // $('#profit_loss img').attr('src', './assets/svg/buy.svg');
    }
    $('#profit_loss .amount').html(profit_loss + ' ' + base_c);

    if (profit_loss_buy_hold < 0) {
      $('#profit_loss_buy_hold .amount').removeClass('green').addClass('red');
      // $('#profit_loss_buy_hold img').attr('src', './assets/svg/sell.svg');
    }
    else {
      $('#profit_loss_buy_hold .amount').removeClass('red').addClass('green');
      // $('#profit_loss_buy_hold img').attr('src', './assets/svg/buy.svg');
    }
    $('#profit_loss_buy_hold .amount').html(profit_loss_buy_hold + ' ' + base_c);

    $('.result_preloader').fadeOut(200);
    $('.container .container-body .result .section img').fadeIn(200);

  }
}
