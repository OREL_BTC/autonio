import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  autonio_token;
  user_name
  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
      let uname = JSON.parse(localStorage.autonio_login_token).username;
      this.user_name=uname;
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }
  }

  exchange_change(val) {
    console.log(val);
    $('.add_keys_form').slideDown();
    $('input[name="api"]').val('');
    $('input[name="secret"]').val('');
    if (this.autonio_token.keys.length > 0) {
      $.each(this.autonio_token.keys, (i, val1) => {
        if (val1.name == val) {
          $('input[name="api"]').val(val1.api);
          $('input[name="secret"]').val(val1.secret);
        }
      });
    }
  }
  Addkeys1(){
    var exc= $('select[name="exchange"]').val();
    var api= $('input[name="api"]').val();
    var sec= $('input[name="secret"]').val();
    var pass=$('input[name="pass1"]').val();
    if(!pass || !sec || !api){
      alert("Some Missing Fields, Please try again");
    }
    else{
      if (this.autonio_token.keys.length > 0) {
        $.each(this.autonio_token.keys, (i, val1) => {
          if (val1.name == exc ) {
            val1.api = api.trim();
            val1.secret = sec.trim();
            val1.password =pass.trim();
            alert('Key Updated Successfully');
          }
        });
     }
      localStorage.setItem('autonio_login_token', JSON.stringify(this.autonio_token));
   }
}
  Addkeys(exc, api, secret) {
    if (this.autonio_token.keys.length > 0) {
      $.each(this.autonio_token.keys, (i, val1) => {
        if (val1.name == exc) {
          val1.api = api.trim();
          val1.secret = secret.trim();
          alert('Key Updated Successfully');
        }
      });
    }
    // console.log(this.autonio_token);

    localStorage.setItem('autonio_login_token', JSON.stringify(this.autonio_token));
    // console.log(JSON.parse(localStorage.getItem('autonio_login_token')));

  }
  checkUpdate(){
    let current=0;
    let uname1=this.user_name;
    let value=0;
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_update_info.php',
      data: { 'uname': uname1 },
      error: (data) => {
      },
      success: (data) => {
        value=parseInt(data);

        console.log("update info from backend==",data,current);
        if(value>current){
          if (window.confirm('New version is available.Press Ok to download new version for more new features,Thankyou'))
            {
              window.open('https://autonio.foundation/autonio_new/index.php#download','_blank');
            }

        }
        else if( value==current){
          alert("You are using updated version. ");
        }
      }
    });

  }
  logout() {
    localStorage.clear();
    console.log(localStorage.getItem('autonio_login_token'));

    this.router.navigateByUrl('/login');
  }

}
