// import { BacktestComponent } from './../backtest/backtest.component';
import { WebService } from './../../services/web.service';
import { Component, OnInit, OnDestroy,  ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NgStyle } from '@angular/common';
import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';
import * as BitShares from 'btsdex';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit, OnDestroy {
  public href: string = "";
  exchanges_tv;
  asset_tv:string[];
  base_tv:string[];
  exchanges: string[];
  pairs: string[];
  pair_detail: string[];
  indicators: string[];
  asset = 'BTC';
  base = 'USD';
  ti = ti1['window'];
  final_buy_sell = [];
  maxDate = new Date();
  private chart: AmChart;
  chart_config;
  chartData;
  lastsl = 0;
  lasttp = 0;
  slstate = 0;
  tpstate = 0;
  live_interval=0;
  asset_bal: number;
  base_bal: number;
  profit_loss: number;
  profit_loss_perc;
  start_bal: number;
  end_bal: number;
  start_asset: number;
  start_base: number;
  profit_loss_buy_hold: number;
  res;
  base_c;
  asset_c;
  live_mode;
  autonio_token;
  session_dat;
  exchange_ar = [];
  apiCurrent;
  secretCurrent;
  excCurrent;
  ccxt_current;
  final_bal_state;
  exchange_fees;
  trade_count: number = 0;
  show_stats: boolean = false;
  stat_ar = [];
  first_signal = true;
  signal_inverse: boolean = false;
  last_signal;
  order_id=[];

  modal_open_settings: boolean = false;
  sellMode: string = 'current';
  blurCss = { 'filter': 'blur(0px)' };
  custom_settings: boolean = false;
  new_settings: boolean = false;
  pairs_modal;
  //for manual buy sell options
  pairs_selected:boolean=false;
  modal_open_advice_manual: boolean = false;
  manual_buy:boolean = false;
  manual_sell:boolean= false;
  modal_open_advice: boolean = false;
  //for implementation of market Maker
  bot_options = [
      { name: "Non Stop Market Maker", s_name: "o_1" },
      { name: "Straight Shooter", s_name: "o_2" },
      { name: "Smart Market Maker", s_name: "o_3" }
    ];
  final_indicators = [];
  asset_s;
  base_s;
  portfolio_s;
  startdate_s;
  enddate_s;
  ind_int_s;
  exc_fee_s;
  pair_s;
  exc_s ='bittrex';
  sl_s;
  tp_s;
  livemode_s;
  wsString = "wss://bitshares.openledger.info/ws";
// proxy = 'http://cors-anywhere.herokuapp.com/';
  proxy = "https://cors-anywhere.herokuapp.com/";
  timeout = 30000;
  // modal_open_advice: boolean = false;
  loading;
  market_stat_msg = [];
  order_stat_msg=[];
  marketmakerInterval;
  checkInterval:number=0;
  last_buy_id;
  last_sell_id;
  main_s_bot;
  select_spread = "s";
  spread_s: number = 4;
  spread_s_s: number = 0.0001;
  refresh_s;
  portfolio_mm_s;
  portfolio_mm_s_from = 10;
  portfolio_mm_s_to = 40;
  select_portfolio_type = "normal";
  base_balance;
  asset_balance;
  amount_to_use;
  base_precision;
  asset_precision;
  botType = "normal";
  isBuy = true;
  isSell = true;
  wait_for_buy = true;
  wait_for_sale = true;
  isBuyFilled = true;
  isSellFilled = true;
  market_maker_session=[];
  asset_val;
  base_val;
  start_maker_bal;
  start_maker_bal_asset;
  start_maker_bal_base;
  end_maker_bal_asset;
  end_maker_bal_base;
  end_maker_bal;
  maker_profit_loss;
  maker_profit_loss_buy_hold;
  strt_asst;
  end_asst;
  asst_in_wallet;
  fillled_order=[];
  buyvol:number=0;
  sellvol:number=0;
  noof_tran:number=0;
  strt_date;
  end_date;
  form_dat;
  //for session
  market_maker_session_data=[];
  // end_bal;
  // maker_data;
  // market_stat_msg;
  // start_bal;
  // start_date;
  // stop_date;
  //for BitShares
  exchange_mm=[];
  base_id;
  asset_id;
  api='';
  account = '';
  password='';
  //for dropdown box.
  config = {
    displayKey: "", //if objects array passed which key to be displayed defaults to description
    search: true,
    placeholder:'--Select One--',
    height:'300px'
  };
  constructor(private router: Router, private activatedRoute : ActivatedRoute, private webservice: WebService, private http: Http, private _elRef: ElementRef, private AmCharts: AmChartsService , private cdRef:ChangeDetectorRef) { }
  ngOnInit() {
    // BitShares.init(this.wsString);
    BitShares.connect();

    this.GenerateStartChart('bittrex');
    this.checkBlur();
    this.ExchangeChange('bittrex');

    $(() => {
      $(".frm_inner_group .parameters").slideUp();
    });

    if (localStorage.getItem('autonio_login_token') == null || localStorage.getItem('autonio_login_token') == "undefined" || localStorage.getItem('autonio_login_token') == undefined) {
      this.router.navigateByUrl('/login');
    }
    else {
      this.autonio_token = JSON.parse((localStorage.getItem('autonio_login_token')));
      this.session_dat=JSON.parse((localStorage.getItem('autonio_session_token')));
      this.market_maker_session=JSON.parse((localStorage.getItem('market_maker_session')));
      console.log("session stored in localstorage==",this.market_maker_session);
    }

    $.each(this.autonio_token.keys, (i, val) => {
      if (val.api != '' && val.secret != '') {
        if(val.name!='bitshares'){
          this.exchange_ar.push(val.name);
        }
          this.exchange_mm.push(val.name);
          if(val.name=='bitshares'){
            this.exchange_mm.push('GDEX','CryptoBridge','EasyDex');
          }
      }
    });

    $(document).on("click", ".checkbox_group input[type='checkbox']", function () {
      if ($(this).prop('checked') == true) {
        $(this).parent().parent().parent().find('.parameters').slideDown();
      }
      else {
        $(this).parent().parent().parent().find('.parameters').slideUp();
      }
    });
  }
  checkBlur() {
    let style;
    if (this.modal_open_settings || this.modal_open_advice || this.modal_open_advice_manual) {
      this.blurCss = { 'filter': 'blur(5px)' };
    }
    else {
      this.blurCss = { 'filter': 'blur(0px)' };
    }
    return style;
  }

  sellModeSet(event) {
    this.sellMode = event.target.value;
  }

  sellSettings(price, sname) {
    var items = [];
    var final_data = [];
    var exchange, pair;
    if (price != '' && price > 0 && sname != '') {
      if (this.sellMode == 'current') {
        items = [];
        final_data = [];
        exchange = $('form#live select[name="exchange"]').val();
        pair = $('form#live select[name="pair"]').val();
        $('.toggler input[type="checkbox"]').each(function () {
          if ($(this).prop('checked')) {
            var id = $(this).attr('id');
            items.push(id);
          }
        });
        $.each(items, function (i, val) {
          var param_ar = [];
          var obj = {};
          $('.frm_inner_group.' + val + ' .parameters input').each(function () {
            var name = $(this).attr('name');
            var val1 = $(this).val();
            var obj1 = {};
            obj1[name] = val1;
            param_ar.push(obj1);
          });
          obj[val] = param_ar;
          final_data.push(obj);
        });
      }
      else {
        items = [];
        final_data = [];
        exchange = $('.sell_settings .modal select[name="exchange"]').val();
        pair = $('.sell_settings .modal select[name="pair"]').val();
        $('.modal_overlay .indicators_settings .toggler input[type="checkbox"]').each(function () {
          if ($(this).prop('checked')) {
            var id = $(this).attr('id');
            items.push(id);
          }
        });
        $.each(items, function (i, val) {
          // console.log(val);
          var param_ar = [];
          var obj = {};
          $('.indicators_settings .' + val + ' .parameters input').each(function () {
            var name = $(this).attr('name');
            var val1 = $(this).val();
            var obj1 = {};
            obj1[name] = val1;
            param_ar.push(obj1);
          });
          obj[val] = param_ar;
          final_data.push(obj);
        });
      }
      if (final_data.length > 0) {
        let data = {
          'username': this.autonio_token.username,
          'settings': JSON.stringify(final_data),
          'price_usd': price,
          'exchange': exchange,
          'pair': pair,
          'sname': sname
        };

        $.ajax({
          url: 'https://auton.io/webservices/user_sell_settings.php',
          method: 'POST',
          data: data,
          success: (data1) => {
            data1 = parseInt(data1);
            switch (data1) {
              case 400:
                this.electron_er('Success');
                this.modal_open_settings = false;
                this.checkBlur();
                break;

              case 202:
                this.electron_er('You already have same settings for sale.');
                break;

              case 404:
                this.electron_er('Some error occured, possibly server problem or your internet connection problem. Please try again after sometime.');
                break;
            }
          }
        });
      }
      else {
        // console.log("from 241 line");
        this.electron_er('Please Select at least one Indicator.');
      }
    }
    else {
      this.electron_er('There is some error with Name or Price. Please recheck both.');
    }
  }

  sellAdvice(price, sname, email) {
    if (price == '' || price <= 0) {
      this.electron_er('Please Select Appropriate Price.');
    }
    else if (sname == '') {
      this.electron_er('Please Enter Your Name.');
    }
    else if (email == '') {
      this.electron_er('Please Enter Your Proper Email.');
    }
    else {
      let sell_advice_data = {
        'username': this.autonio_token.username,
        'sname': sname,
        'email': email,
        'price': price
      }
      $.ajax({
        url: 'https://auton.io/webservices/user_sell_advice.php',
        method: 'POST',
        data: sell_advice_data,
        success: (data) => {
          data = parseInt(data);
          console.log("From sell advice==",data);
          switch (data) {
            case 400:
              this.electron_er('Success.');
              this.modal_open_advice = false;
              this.checkBlur();
              break;

            case 404:
              this.electron_er('There is some error. Please Try again later.');
              break;
          }
        }
      });
    }
  }
  ngAfterViewInit(){
    var scope=this;
   var pairval;
   var count=0;
   if(this.live_interval){
       clearInterval(this.live_interval);
   }
   var dataLiveForm = $.parseJSON(localStorage.tab_backtest_data);
   if (dataLiveForm.length != 0 && dataLiveForm.length>0) {
     for(var i = 0; i < dataLiveForm.length; i++) {
       var singleCell = dataLiveForm[i];

       if(singleCell.name=="exchange") {
           $.each(this.exchange_ar,function(i,val){
             if(singleCell.value==val){
               $('[name='+singleCell.name+']', 'form').val(singleCell.value);
               $('[name='+singleCell.name+']', 'form').change();
               scope.ExchangeChange(singleCell.value);
               count+=1;
             }
           });

          if(count<1){
            this.electron_er("Please add apiKeys for respective exchanges and try again. Thankyou");
            var dataBacktestForm = '{ "tab_backtest_data": 0 }';
            localStorage.setItem('tab_backtest_data', dataBacktestForm);
            this.router.navigateByUrl('/saved_algorithm');
          }

       } else if(singleCell.name=="pair") {
       } else {
         if($('[name='+singleCell.name+']', 'form').attr('type')=="checkbox") {
         } else {
           $('[name='+singleCell.name+']', 'form').val(singleCell.value);
         }
       }
     }

     var liveTabPair=setInterval(function () {
       if($('[name="pair"]', 'form').children('option').length>1){
         clearInterval(liveTabPair);
         if(dataLiveForm[1].name=="pair"){
           $('[name="pair"]', 'form').val(dataLiveForm[1].value);
           $('[name='+dataLiveForm[1].name+']', 'form').trigger("change");
           //this.PairChange($('[name="exchange"]', 'form').val(),dataLiveForm[1].value);
         }
       }
     }, 300);
     var liveTabInterval=setInterval(function () {
       if($('.toggler input[type="checkbox"]').length>10){
         clearInterval(liveTabInterval);
         for(var i = 0; i < dataLiveForm.length; i++) {
           var singleCell = dataLiveForm[i];
           if(singleCell.name=="exchange") {
           } else if(singleCell.name=="pair") {
           } else {
             if($('[name='+singleCell.name+']', 'form').attr('type')=="checkbox") {
               //$('[name='+singleCell.name+']', 'form').prop('checked', true);
               $('[name='+singleCell.name+']', 'form').trigger("click");
             } else {
               $('[name='+singleCell.name+']', 'form').val(singleCell.value);
             }
           }
         }
         /*var tab_live_trade=$.parseJSON(localStorage.tab_live_trade);
         if(tab_live_trade.tab_live_trade==1) {
           $('.start_trade').click();
         } else {
           $('.stop_trade').click();
         }*/
       }
     }, 300);
     if(dataLiveForm[1].name=="pair"){
       pairval= (dataLiveForm[1].value);
       var pos = pairval.indexOf("/");
       this.asset=pairval.slice(0,pos);
       this.base=pairval.slice(pos+1,pairval.length);
       this.cdRef.detectChanges();
     }
   }

 }
  checkUser1(uname) {
    $.ajax({
      url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address='+ uname+'&tag=latest&apikey=YourApiKeyToken',
      method: 'POST',
      data: { 'username': uname },
      success: (data) => {
        let received_amnt = parseInt(data.result);
       if(received_amnt<3000){
         this.router.navigateByUrl('/membership');
       }
   },
   error: (data) => {
       this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');

      }
    });
  }
  //to check account portfilio
  portfolio(){
    this.router.navigateByUrl('/balance');
  }
  //to check saved algorithm
  myalgo(){
    this.router.navigateByUrl('/saved_algorithm');
  }

  InverseToggle() {
    this.signal_inverse = !this.signal_inverse;
    // console.log(this.signal_inverse);
  }

  UcFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  public ReArrange(ar) {
    let last = '';
    let final_ar = [];
    $.each(ar, (i, val) => {
      if (val != last && val != '' && val != undefined) {
        final_ar[i] = val;
        last = val;
      }
      else {
        final_ar[i] = '';
      }
    });
    return final_ar;
  }

  toMinute(date) {
    let d1 = (mnt(date).format('YYYY-MM-DD HH:mm'));
    return (this.toTimestamp(d1));
  }

  GenerateStartChart(exc) {
    var scope=this;
    if(exc=='bitshares' || exc== 'GDEX' || exc== 'CryptoBridge' || exc== 'EasyDex'){
      $('.chart_preloader').fadeIn(200);
      BitShares.subscribe('connected',
      async () => {
        let temp_ohlc_ar = [];
        let interval = 3600;
        let duration: any = 7;
        var startDate = mnt(new Date()).subtract(duration, "Days");
        var endDate = new Date();
        let start = mnt(startDate).utc().format("YYYY-MM-DDTHH:mm:ss");
        let end = mnt(endDate).utc().format("YYYY-MM-DDTHH:mm:ss");
        let base_precision;
        let asset_precision;
        await BitShares.db.list_assets(this.base, 1).then(data => {
          this.base_id = data[0]['id'];
          base_precision = 10 ** (data[0]['precision']);
        });
        await BitShares.db.list_assets(this.asset, 1).then(data => {
          this.asset_id = data[0]['id'];
          asset_precision = 10 ** (data[0]['precision'] - 2);
        });
        await BitShares.history.get_market_history(this.base_id, this.asset_id, interval, start, end).then(data => {
          $.each(data, (i, val) => {
            let open_quote = val["open_quote"];
            let high_quote = val["high_quote"];
            let low_quote = val["low_quote"];
            let close_quote = val["close_quote"];
            let volume_quote = val["quote_volume"];
            let open_base = val["open_base"];
            let high_base = val["high_base"];
            let low_base = val["low_base"];
            let close_base = val["close_base"];
            let volume_base = val["base_volume"];
            let time = val["key"]["open"] + '.000Z';
            let timestamp = new Date(time);
            let open = 1 / ((open_base / base_precision) / (open_quote / asset_precision));
            let high = 1 / ((high_base / base_precision) / (high_quote / asset_precision));
            let low = 1 / ((low_base / base_precision) / (low_quote / asset_precision));
            let close = 1 / ((close_base / base_precision) / (close_quote / asset_precision));
            let volume = 1 / ((volume_base / base_precision) / (volume_quote / asset_precision));

            let chart = { 'date': this.formatDate(timestamp.toString()), 'value': close };

            temp_ohlc_ar.push(chart);

          });
          if(temp_ohlc_ar.length>0){
            this.chart_g(temp_ohlc_ar);
          }
        });
      }
    );
    }
    else{
      let a, b;
      if (this.asset == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex") {
        a = 'USD';
      }
      else {
        //to solve pair doesnot exist problem while user selects ethfinex.
        if(exc=='ethfinex' && this.asset=='BTC'){
          this.asset='ETH';
        }
        a = this.asset;
      }
      if (this.base == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex") {
        b = 'USD';
      }
      else {
        b = this.base;
      }
      let options = { aggregate: 1, timestamp: new Date(), exchange: exc };
      $('.chart_preloader').fadeIn(200);

      cc.histoMinute(a, b, options)
      .then(data => {
        // this.chart.clear();
        this.chart = null;

        let chartData = '[';
        $.each(data, (i, val) => {
          //console.log("Val[time]for chart data",val);
          let timeStamp = new Date(val['time'] * 1000);
          let formated_date = this.formatDate(timeStamp.toString());
          chartData += '{"date" : "' + formated_date + '", "value" : ' + val['close'] + '}';
          if (i != data.length - 1) {
            chartData += ',';
          }
        });
        chartData += ']';
        let cd = JSON.parse(chartData);
        let chart_config = {
          "type": "serial",
          "theme": "dark",
          "addClassNames": true,
          "dataProvider": cd,
          "dataDateFormat": "YYYY-MM-DD HH:NN",
          "categoryField": "date",
          "creditsPosition": "bottom-right",
          "valueAxes": [{
            "axisAlpha": 0.2,
            "dashLength": 1,
            "position": "left"
          }],
          "defs": {
            "filter": {
              "id": "dropshadow",
              "x": "-10%",
              "y": "-10%",
              "width": "120%",
              "height": "120%",
              "feOffset": {
                "result": "offOut",
                "in": "rgba",
                "dx": "3",
                "dy": "3"
              },
              "feGaussianBlur": {
                "result": "blurOut",
                "in": "offOut",
                "stdDeviation": "5"
              },
              "feBlend": {
                "in": "SourceGraphic",
                "in2": "blurOut",
                "mode": "normal"
              }
            }
          },
          "graphs": [{
            "id": "g1",
            "type": "smoothedLine",
            "bulletSize": 28,
            // "customBullet": '',
            "customBulletField": "customBullet",
            "balloonText": "[[value]]",
            "lineColor": "var(--blue-2)",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 0,
            "valueField": "value",
            "useLineColorForBulletBorder": true,
            "lineThickness": 2
            // "balloon":{
              //     "drop":true
              // }
            }],
            "chartCursor": {
              "categoryBalloonDateFormat": "HH:NN, DD MMMM",
              "cursorPosition": "mouse"
            },
            "chartScrollbar": {
              "autoGridCount": true,
              "graph": "g1",
              "scrollbarHeight": 40
            },
            "categoryAxis": {
              "minPeriod": "mm",
              "parseDates": true,
              "axisColor": "#DADADA",
              "dashLength": 1,
              "minorGridEnabled": true
            }
          };

          this.chart = this.AmCharts.makeChart("chart", chart_config);
          $('.chart_preloader').fadeOut(200);
        })
    }
  }

  chart_g(cd) {
   let chart_config = {
     "type": "serial",
     "theme": "dark",
     "addClassNames": true,
     "dataProvider": cd,
     "dataDateFormat": "YYYY-MM-DD HH:NN",
     "categoryField": "date",
     "creditsPosition": "bottom-right",
     "valueAxes": [{
       "axisAlpha": 0.2,
       "dashLength": 1,
       "position": "left"
     }],
     "defs": {
       "filter": {
         "id": "dropshadow",
         "x": "-10%",
         "y": "-10%",
         "width": "120%",
         "height": "120%",
         "feOffset": {
           "result": "offOut",
           "in": "rgba",
           "dx": "3",
           "dy": "3"
         },
         "feGaussianBlur": {
           "result": "blurOut",
           "in": "offOut",
           "stdDeviation": "5"
         },
         "feBlend": {
           "in": "SourceGraphic",
           "in2": "blurOut",
           "mode": "normal"
         }
       }
     },
     "graphs": [{
       "id": "g1",
       "type": "smoothedLine",
       "bulletSize": 28,
       // "customBullet": '',
       "customBulletField": "customBullet",
       "balloonText": "[[value]]",
       "lineColor": "var(--blue-2)",
       "bulletBorderAlpha": 1,
       "bulletColor": "#FFFFFF",
       "hideBulletsCount": 0,
       "valueField": "value",
       "useLineColorForBulletBorder": true,
       "lineThickness": 2
       // "balloon":{
         //     "drop":true
         // }
       }],
       "chartCursor": {
         "categoryBalloonDateFormat": "HH:NN, DD MMMM",
         "cursorPosition": "mouse"
       },
       "chartScrollbar": {
         "autoGridCount": true,
         "graph": "g1",
         "scrollbarHeight": 40
       },
       "categoryAxis": {
         "minPeriod": "mm",
         "parseDates": true,
         "axisColor": "#DADADA",
         "dashLength": 1,
         "minorGridEnabled": true
       }
  };

  this.chart = this.AmCharts.makeChart("chart", chart_config);
  $('.chart_preloader').fadeOut(200);
}

  formatDate(date) {
    return mnt(date).format('YYYY-MM-DD HH:mm');
  }

  public getJSON(url) {
    return this.http.get(url).map(res => res.json());
  }

  PairChange(exc, pair) {
    if(exc != 'bitshares' && exc != 'GDEX'&& exc != 'CryptoBridge' && exc != 'EasyDex'){
      (async () => {
        let val1 = new ccxt[exc];
        await val1.loadMarkets();
        let pair1 = val1.markets[pair];
        this.asset = pair1['base'];
        this.asset_tv= pair1['base'];
        this.base_tv=pair1['quote'];
        this.base = pair1['quote'];
        this.GenerateStartChart(exc);
      })()
    }
    else{
      let pair_ar = pair.split('/');
      this.asset = pair_ar[0];
      this.base = pair_ar[1];
      // this.pairs_selected=true;
      this.GenerateStartChart(exc);
    }
    this.pair_s=pair;

  }

  ExchangeChange(exc) {
    if(exc=='ethfinex'){
      this.exchanges_tv="BITFINEX";
    }
    else{
      this.exchanges_tv=exc.toUpperCase();
    }
    if(exc != 'bitshares' && exc != 'GDEX'&& exc != 'CryptoBridge' && exc != 'EasyDex'){
      this.asset = 'BTC';
      this.base = 'USD';
      (async () => {
        let val1 = new ccxt[exc];
        let pair_detail = await val1.load_markets();
        this.pairs = Object.keys(pair_detail);
        this.pairs.sort();
        this.GenerateStartChart(exc);
      })()
    }
    else {
      switch (exc){
        case 'bitshares':
          this.pairs=[
            'BTS/CNY',
            'BTS/OBITS',
            'BTS/RUBLE',
            'BTS/USD',
            'CNY/USD',
            'OPEN.LTC/BTS',
            'USD/CNY',
            'USD/RUBLE'
          ];
          this.asset = 'BTS';
          this.base = 'CNY';
          break;
        case 'GDEX':
          this.pairs=[
            'GDEX.BTC/CNY',
            'GDEX.EOS/CNY',
            'GDEX.ETH/CNY',
            'GDEX.BTC/USD',
            'GDEX.EOS/USD',
            'GDEX.ETH/USD',
            'GDEX.BTC/BTS',
            'GDEX.BTC/GDEX.EOS',
            'GDEX.BTC/GDEX.ETH',
            'GDEX.NEO/CNY',
            'XGDEX.GAS/CNY',
            'GDEX.QTUM/CNY',
            'GDEX.SEER/CNY',
            'GDEX.BKBT/CNY'
          ];
          this.asset = 'GDEX.BTC';
          this.base = 'CNY';
          break;
        case 'CryptoBridge':
          this.pairs=[
            'BRIDGE.BTC/BRIDGE.NIO',
            'USD/BRIDGE.BCO',
            'USD/BRIDGE.BTC',
            'USD/BRIDGE.LTC',
            'CNY/BRIDGE.BCO',
            'CNY/BRIDGE.LTC',
            'CNY/BRIDGE.DASH'
          ];
          this.asset='CNY';
          this.base='GDEX.ETH';
          break;
        case 'EasyDex' :
          this.pairs=[
            'BTS/EASYDEX.US',
            'BTS/EASYDEX.EU',
            'BTS/EASYDEX.BTC',
            'BTS/EASYDEX.PB',
            'CNY/EASYDEX.US',
            'CNY/EASYDEX.EU',
            'CNY/EASYDEX.BTC',
            'CNY/EASYDEX.PB',
            'USD/EASYDEX.US',
            'USD/EASYDEX.EU',
            'USD/EASYDEX.BTC',
            'USD/EASYDEX.PB'
          ];
          this.asset='BTS';
          this.base='EASYDEX.BTC';
          break;
      }
      this.GenerateStartChart(exc);
    }
    this.exc_s=exc;
  }
  ExchangeChangeModal(exc) {
    (async () => {
      let val1 = new ccxt[exc];
      let pair_detail = await val1.load_markets();
      this.pairs_modal = Object.keys(pair_detail);
    })()
  }
  StatToggle() {
    this.show_stats = !this.show_stats;
    // console.log(this.show_stats);
  }
  StopTrade() {
    var tab_live_trade = '{ "tab_live_trade": 0 }';
    localStorage.setItem('tab_live_trade', tab_live_trade);
    let d = new Date();
    this.session_dat.stop_date=d.getTime();
    // console.log('Trade Stop');
    if(this.live_interval){
      // console.log("interval clear hogaya...");
      clearInterval(this.live_interval);
    }
    $('.page-body #live .overlay').fadeOut(200);
    this.final_buy_sell = [];
    this.chartData = '';
    this.trade_count = 0;
    this.first_signal = true;
    $('.stop_trade').css('opacity', '0.3').css('cursor', 'not-allowed').attr('disabled', true);
    $('.start_trade').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
    $('.check_my_funds').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
    var dataBacktestForm = '{ "tab_backtest_data": 0 }';
    localStorage.setItem('tab_backtest_data', dataBacktestForm);
  }

  CheckBal(asset, base, fn) {
    let final_return;
    (async () => {
      let fetchBalance = await this.ccxt_current.fetchBalance({'recvWindow': 10000000});
      // console.log("checkBal fun log 1===",fetchBalance.free);
      this.session_dat.information.push(fetchBalance);
      if (fetchBalance.free) {
        let fetchFreeBalance = (fetchBalance.free);
        let status_b = 0, status_a = 0, asset_exist = 1, base_exist = 1;
        if (!(this.asset_c in fetchFreeBalance) && asset > 0) {
          asset_exist = 0;
          this.electron_er('You have unsufficient ' + this.asset_c + ' balance, Please check your exchange.');
        }
        else if (!(this.asset_c in fetchFreeBalance)) {
          this.electron_er("You don't have " + this.asset_c + " registered in your Exchange");
        }
        if (!(this.base_c in fetchFreeBalance) && base > 0) {
          base_exist = 0;
          this.electron_er('You have unsufficient ' + this.base_c + ' balance, Please check your exchange.');
        }
        else if (!(this.base_c in fetchFreeBalance)) {
          this.electron_er("You don't have " + this.base_c + " registered in your Exchange");
        }
        if (asset_exist == 1 && base_exist == 1) {
          $.each(fetchFreeBalance, (i, val) => {
            if (i == this.base_c) {
              if (base > val) {
                this.electron_er('You have unsufficient amount of ' + i);
              }
              else {
                status_b = 1;
              }
            }
            if (i == this.asset_c) {
              if (asset > val) {
                this.electron_er('You have unsufficient amount of ' + i);
              }
              else {
                status_a = 1;
              }
            }
          });
          if (status_a == 1 && status_b == 1) {
            this.final_bal_state = true;
          }
          else {
            this.final_bal_state = false;
          }
        }
        else {
          this.final_bal_state = false;
        }
      }
      fn();
    })().catch(alert);
  }
  electron_er(msg) {
    alert(msg);
  }
  toTimestamp(strDate) {
    var dt = new Date(strDate);
    var datum = dt.getTime();
    return (datum / 1000).toFixed(0);
  }
  Live() {
    var pair, interval, after, before, period, exchange;

    let uname = this.autonio_token.username;
    this.checkUser1(uname);
    this.asset_bal = 0;
    this.base_bal = 0;
    this.start_bal = 0;
    this.end_bal = 0;
    this.profit_loss = 0;
    this.profit_loss_perc = 0;
    this.profit_loss_buy_hold = 0;
    this.stat_ar = [];
    this.last_signal = '';
    $(document).ready(() => {
      // Date.prototype.getUnixTime = function () { return this.getTime() / 1000 | 0 };
      pair = $('select[name="pair"]').val();
      interval = parseFloat($('select[name="ind_int"]').val());
      exchange = $('select[name="exchange"]').val();
      var stop_loss = parseFloat($('input[name="stop_loss"]').val());
      var take_profit = parseFloat($('input[name="take_profit"]').val());
      var portfolio_perc = parseFloat($('input[name="portfolio"]').val());
      this.asset_bal = parseFloat($('input[name="asset"]').val());
      this.base_bal = parseFloat($('input[name="base"]').val());
      this.live_mode = $('select[name="live_mode"]').val();
      this.exchange_fees = $('input[name="fees"]').val();
      this.start_asset = this.asset_bal;
      this.start_base = this.base_bal;
      this.base_c = this.base.toUpperCase();
      this.asset_c = this.asset.toUpperCase();
      var portfolio = parseFloat(this.PerCalc(this.start_base, portfolio_perc).toFixed(8));
      if (pair == null || interval == null || exchange == null || stop_loss > 100 || take_profit > 100 || portfolio_perc > 100 || (this.start_asset <= 0 && this.start_base <= 0)) {
        this.electron_er('Some Missing Fields, Please try again.');
      }
      else {
        var tab_live_trade = '{ "tab_live_trade": 1 }';
        localStorage.setItem('tab_live_trade', tab_live_trade);
        $('.check_my_funds').css('opacity', '0.3').css('cursor', 'not-allowed').attr('disabled', true);
        $.each(this.autonio_token.keys, (i, val) => {
          if (val.name == exchange) {
            this.apiCurrent = val.api;
            this.secretCurrent = val.secret;
            if(val.password){
              this.password=val.password;
            }
          }
        });
        if (this.live_mode == 'live') {
          if (this.start_base > 0) {
            this.excCurrent = exchange;
            $.ajax({
              url: 'https://min-api.cryptocompare.com/data/price?fsym=' + this.asset_c + '&tsyms=BTC',
              success: (data) => {
                let asset_check: number = parseFloat((this.asset_bal * data['BTC']).toFixed(8));
                if (asset_check <= 0.0005 && this.start_asset > 0) {
                  this.electron_er('Minimum amount of ' + this.asset_c + ' should match 0.0005 BTC');
                  this.StopTrade();
                }
                else {
                  //for build doesnot required proxy
                  // this.ccxt_current.proxy = this.proxy;
                  this.ccxt_current = new ccxt[this.excCurrent];
                  this.ccxt_current.apiKey = this.apiCurrent;
                  this.ccxt_current.secret = this.secretCurrent;
                  if(exchange=='kucoin' ){
                  this.ccxt_current.password=this.password;
                  }
                  //to store session of initial live trade time
                  let d = new Date();
                  this.session_dat.start_date=d.getTime();
                  this.session_dat.trade_data=JSON.stringify($('form').serializeArray());
                  this.CheckBal(this.start_asset, this.start_base, () => {
                    if (this.final_bal_state) {

                      after = this.toMinute(new Date());
                      before = 'none';
                      period = interval;
                      $('.page-body #live .overlay').fadeIn(200);
                      this.GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, portfolio);
                      this.live_interval = setInterval(() => {
                        this.GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, portfolio);
                      }, 60000);
                    }
                  });
                }
              }
            });
            // to check profit and give option to sell algo
            var data_trade = {
              'username': this.autonio_token.username,
              'start': this.start_bal,
              'end': this.end_bal,
              'p_l': this.profit_loss,
              'p_l_b_h': this.profit_loss_buy_hold,
              'base': this.base_c
            };
            $.ajax({
              method: 'POST',
              url: 'https://auton.io/webservices/user_trade_new.php',
              data: data_trade,
              success: (data1) => {

                if (data1 == '400') {
                  var data = {
                    'username': this.autonio_token.username
                  };
                  $.ajax({
                    method: 'POST',
                    url: 'https://auton.io/webservices/user_get_profit_perc.php',
                    data: data,
                    success: (perc) => {
                      perc = parseFloat(perc);
                      if (perc >= 100 && perc != 404) {
                        this.modal_open_advice = true;
                        this.checkBlur();
                      }
                      else if (perc >= 10 && perc != 404) {
                        if (window.confirm(" If you want to sell your algorithm you need to stop trading.")) {
                          this.StopTrade();
                          this.sellMode = 'current';
                          this.modal_open_settings = true;
                          this.checkBlur();
                          this.new_settings = false;
                        }
                        else{
                          return false;
                        }
                      }
                    }
                  });
                }
              }
            });
          }
          else {
            this.electron_er('Please add some ' + this.base_c);
          }
        }
        else {
          after = this.toMinute(new Date());
          before = 'none';
          period = interval;
          $('.page-body #live .overlay').fadeIn(200);
          this.GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, portfolio);
          this.live_interval = setInterval(() => {
            this.GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, portfolio);
          }, 60000);
        }
      }
    });
  }
  round(value, decimals) {
    return (Math.round(value), decimals);
  }
  PerCalc(a, b) {

    return parseFloat(((a / 100) * b).toFixed(8));
  }
  MakeChart(timestamp, cp, stop_loss, take_profit, portfolio) {
    this.start_bal = parseFloat(((this.start_asset * cp[0]) + this.start_base).toFixed(8));
    var buy = 0, sell = 0;
    $.ajax({
      url: 'https://min-api.cryptocompare.com/data/price?fsym=' + this.base_c + '&tsyms=BTC',
      success: (data1) => {
        let base_check: number = portfolio * data1['BTC'];
        if (base_check <= 0.001) {
          this.electron_er('Minimum amount of ' + this.base_c + ' should match 0.001 BTC');
          this.StopTrade();
        }
        else {
          this.chart.clear();
          this.chart = null;
          let chartData = '[';

          $.each(this.final_buy_sell, (i, val) => {
            let timeStamp = new Date(timestamp[i] * 1000);
            let formated_date = this.formatDate(timeStamp.toString());
            let current_time = this.formatDate(new Date().toString());
            if (val == 'BUY') {
              if (formated_date == current_time && this.last_signal != 'BUY') {
                this.last_signal = 'BUY'
                this.lastsl = (cp[i] * ((100 - parseFloat(stop_loss)) / 100));
                let lasttp2 = (cp[i] * ((100 + parseFloat(take_profit)) / 100));
                this.slstate = 1;
                this.tpstate = 1;

                var final_buy_val_1 = parseFloat(((this.base_bal / portfolio)).toFixed(8));
                var final_buy_val = parseFloat(((portfolio / cp[i])).toFixed(8));

                let symbol = this.asset_c + '/' + this.base_c;
                let amount = final_buy_val;
                let price = cp[i] * 0.6;
                if (this.live_mode == 'live') {
                  // console.log(final_buy_val);

                  this.CheckBal(0, portfolio, () => {
                    if (this.final_bal_state) {

                      //console.log('BUY on ' + symbol + ' of ' + amount + ' when price is ' + price);
                      if (amount > 0) {
                        this.ccxt_current.createLimitBuyOrder(symbol, amount, price);
                          let trade = { 'signal': 'BUY', 'symbol': symbol, 'amount': amount, 'bid': cp[i], 'time': formated_date };
                          this.stat_ar.push(trade);
                          this.session_dat.trade_stat.push(trade);
                          this.trade_count++;
                      }
                      this.asset_bal += amount;
                      sell = 0; buy = 1;
                    }
                    else {
                      this.StopTrade();
                    }
                  });
                }
                else {
                  if (final_buy_val > 0) {
                    this.trade_count++;
                    let trade = { 'signal': 'BUY', 'symbol': symbol, 'amount': amount, 'bid': cp[i], 'time': formated_date };
                    this.stat_ar.push(trade);
                  }
                  this.asset_bal += amount;
                  sell = 0; buy = 1;
                }
                this.base_bal = this.base_bal - portfolio;
              }
                chartData += '{"date" : "' + formated_date + '", "value" : ' + cp[i] + ', "customBullet" : "assets/svg/buy.svg"}';
            }
            else if (val == 'SELL') {
              if (formated_date == current_time && this.last_signal != 'SELL') {
                this.last_signal = 'SELL'
                let symbol = this.asset_c + '/' + this.base_c;
                var final_sell_val = parseFloat((cp[i] * this.asset_bal).toFixed(8));
                if (this.live_mode == 'live') {
                  this.CheckBal(this.asset_bal, 0, () => {
                    if (this.final_bal_state) {
                      let amount = this.asset_bal;
                      // console.log(amount);
                      let price = cp[i] * 1.3;
                      console.log('SELL on ' + symbol + ' of ' + amount + ' worth ' + final_sell_val + ' when price is ' + price);
                      if (amount > 0) {
                         this.ccxt_current.createLimitSellOrder(symbol, amount, price);
                          let trade = { 'signal': 'SELL', 'symbol': symbol, 'amount': this.asset_bal, 'bid': cp[i], 'time': formated_date };
                          this.stat_ar.push(trade);
                          this.session_dat.trade_stat.push(trade);
                          this.trade_count++;
                      }
                      this.base_bal += final_sell_val;
                      this.asset_bal = 0;
                      sell = 1; buy = 0;
                    }
                    else {
                      this.StopTrade();
                    }
                  });
                }
                else {

                  let price = cp[i] * 0.9;
                  if (this.asset_bal > 0) {
                    this.trade_count++;
                    let trade = { 'signal': 'SELL', 'symbol': symbol, 'amount': this.asset_bal, 'bid': cp[i], 'time': formated_date };
                    this.stat_ar.push(trade);
                  }
                  this.base_bal += final_sell_val;
                  this.asset_bal = 0;
                  sell = 1; buy = 0;
                }
              }
                chartData += '{"date" : "' + formated_date + '", "value" : ' + cp[i] + ', "customBullet" : "assets/svg/sell.svg"}';

            }
            else {
              chartData += '{"date" : "' + formated_date + '", "value" : ' + cp[i] + '}';
            }
            if (i < this.final_buy_sell.length - 1) {
              chartData += ',';
            }
          });
          chartData += ']';
          this.chartData = (JSON.parse(chartData));
          this.end_bal = parseFloat(((this.asset_bal * cp[cp.length - 1]) + this.base_bal).toFixed(8));
          this.end_bal = parseFloat((this.end_bal - ((this.trade_count) * (portfolio) * (this.exchange_fees / 100))).toFixed(8));
          this.profit_loss = parseFloat((this.end_bal - this.start_bal).toFixed(8));
          // console.log(this.end_bal, this.start_asset, this.start_base, cp[cp.length -1]);
          let buy_hold = parseFloat(((this.start_asset * cp[cp.length - 1]) + this.start_base).toFixed(8));

          this.profit_loss_buy_hold = parseFloat((this.end_bal - buy_hold).toFixed(8));
          this.profit_loss_perc = parseFloat(((100 * this.end_bal) / this.start_bal).toFixed(8)) - 100;
          this.base_c = this.base.toUpperCase();
          $('.result_preloader').fadeOut(200);
          $('#chart').css('height', '100%');
          let chart_config = {
            "type": "serial",
            "theme": "dark",
            "addClassNames": true,
            "dataProvider": this.chartData,
            "dataDateFormat": "YYYY-MM-DD HH:NN",
            "categoryField": "date",
            "creditsPosition": "bottom-right",
            "valueAxes": [{
              "axisAlpha": 0.2,
              "dashLength": 1,
              "position": "left"
            }],
            "defs": {
              "filter": {
                "id": "dropshadow",
                "x": "-10%",
                "y": "-10%",
                "width": "120%",
                "height": "120%",
                "feOffset": {
                  "result": "offOut",
                  "in": "rgba",
                  "dx": "3",
                  "dy": "3"
                },
                "feGaussianBlur": {
                  "result": "blurOut",
                  "in": "offOut",
                  "stdDeviation": "5"
                },
                "feBlend": {
                  "in": "SourceGraphic",
                  "in2": "blurOut",
                  "mode": "normal"
                }
              }
            },
            "graphs": [{
              "id": "g1",
              "type": "smoothedLine",
              "bulletSize": 28,
              // "customBullet": '',
              "customBulletField": "customBullet",
              "balloonText": "[[value]]",
              "lineColor": "var(--blue-2)",
              "bulletBorderAlpha": 1,
              "bulletColor": "#FFFFFF",
              "hideBulletsCount": 0,
              "valueField": "value",
              "useLineColorForBulletBorder": true,
              "lineThickness": 2
            }],
            "chartCursor": {
              "categoryBalloonDateFormat": "HH:NN, DD MMMM",
              "cursorPosition": "mouse"
            },
            "chartScrollbar": {
              "autoGridCount": true,
              "graph": "g1",
              "scrollbarHeight": 40
            },
            "categoryAxis": {
              "minPeriod": "mm",
              "parseDates": true,
              "axisColor": "#DADADA",
              "dashLength": 1,
              "minorGridEnabled": true
            }
          };

          this.chart = this.AmCharts.makeChart("chart", chart_config);
          $('.chart_preloader').fadeOut(200);
        }
      }
    });
  }

  GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, portfolio) {
    $(() => {
      var items = [];
      $('.toggler input[type="checkbox"]').each(function () {
        if ($(this).prop('checked')) {
          var id = $(this).attr('id');
          items.push(id);
        }
      });
      var final_data = [];

      $.each(items, function (i, val) {
        var param_ar = [];
        var obj = {};
        $('.frm_inner_group.' + val + ' .parameters input').each(function () {
          var name = $(this).attr('name');
          var val1 = $(this).val();
          var obj1 = {};
          obj1[name] = val1;
          param_ar.push(obj1);
        });

        obj[val] = param_ar;
        final_data.push(obj);
      });

      // console.log("final data with selected indicaters and its parameters==",final_data);

      if (final_data.length != 0) {

        $('.stop_trade').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
        $('.start_trade').attr('disabled', true).css('opacity', '0.3').css('cursor', 'not-allowed');
        $('.stop_trade').attr('disabled', false);
        var timestamp = [];
        var op = [];
        var hp = [];
        var lp = [];
        var cp = [];
        var volume = [];
        var today = parseInt(this.toMinute(new Date()));
        after = parseInt(after);
        let a, b;
        if (this.asset == 'USDT' && exchange != "binance" && exchange != "kucoin" && exchange != "okex") {
          a = 'USD';
        }
        else {
          a = this.asset;
        }
        if (this.base == 'USDT' && exchange != "binance" && exchange != "kucoin" && exchange != "okex") {
          b = 'USD';
        }
        else {
          b = this.base;
        }

        if (period < 60) {
          let options = { aggregate: period, timestamp: new Date(), exchange: exchange };

          cc.histoMinute(a, b, options)
            .then(data => {

              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              let last_time = (data[data.length - 1]['time']);
              if (last_time == today) {
                this.PerformLive(stop_loss, take_profit, portfolio, cp, timestamp, hp, op, lp, volume, final_data)
              }
              else if (this.first_signal) {
                $('.chart_preloader').fadeIn(200);
              }
            })
            .catch(console.error);
        }
        else if (period >= 60 && period < 1440) {
          let options = { aggregate: period / 60, timestamp: new Date(), exchange: exchange };

          cc.histoHour(a, b, options)
            .then(data => {

              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });
              let last_time = (data[data.length - 1]['time']);
              if (last_time == today) {
                this.PerformLive(stop_loss, take_profit, portfolio, cp, timestamp, hp, op, lp, volume, final_data)
              }
              else if (this.first_signal) {
                $('.chart_preloader').fadeIn(200);
              }
            })
            .catch(console.error);
        }
        else {
          let options = { aggregate: period / 1440, timestamp: new Date(), exchange: exchange };

          cc.histoDay(a, b, options)
            .then(data => {

              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });
              let last_time = (data[data.length - 1]['time']);
              if (last_time == today) {
                this.PerformLive(stop_loss, take_profit, portfolio, cp, timestamp, hp, op, lp, volume, final_data)
              }
              else if (this.first_signal) {
                $('.chart_preloader').fadeIn(200);
              }
            })
            .catch(console.error);
        }

      }
      else {
        var item=[];
        $('.toggler input[type="checkbox"]').each(function () {
          if ($(this).prop('checked')) {
            var id = $(this).attr('id');
            item.push(id);
          }
        });
        if(item.length == 0 && this.router.url=='/live'){
          this.electron_er('Please select at least one indicator.');
        }
        // console.log("settime interval kaa status k xa===",this.live_interval);
        this.StopTrade();
      }

    });

  }

  PerformLive(stop_loss, take_profit, portfolio, cp, timestamp, hp, op, lp, volume, final_data) {
    $('.chart_preloader').fadeIn(200);
    this.first_signal = false;
    function array_pos_1(start, ar1, ar2) {
      var ar_return = [];
      for (var i = start, j = 0; i < ar2.length; i++ , j++) {
        if (j < start) {
          ar_return[j] = '';
        }
        ar_return[i] = ar1[j];
      }
      return ar_return;
    }
    var indi_ar = [];
    var indi_name_ar = [];
    var indi_params = [];
    var buy, sell;
    var input, tp;
    $.each(final_data, (i, val) => {
      var indi = (Object.keys(val)[0]);
      $.each(val, function (j, val2) {
        indi_params = val2;
      });
      switch (indi) {
        case 'adl':
          input = { high: hp, low: lp, close: cp, volume: volume };
          var adl_res = this.ti.adl(input);
          var adl_final = [];
          buy = 0;
          sell = 0;
          $.each(adl_res, function (i, val) {
            if (val >= 0 && buy == 0) {
              adl_final[i] = 'BUY';
              buy = 1;
              sell = 0;
            }
            else if (val < 0 && sell == 0) {
              adl_final[i] = 'SELL';
              buy = 0;
              sell = 1;
            }
            else {
              adl_final[i] = '';
            }
          });
          indi_ar.push(adl_final);
          break;

        case 'adx':
          tp = indi_params[0]['adx_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var adx_final = [];
          var adx_res_temp = this.ti.adx(input);
          var adx_res_temp2 = [];
          buy = 0;
          sell = 0;
          $.each(adx_res_temp, function (i, val) {
            if (val['mdi'] >= val['pdi'] && buy == 0) {
              adx_res_temp2.push('BUY');
              buy = 1;
              sell = 0;
            }
            else if (val['mdi'] < val['pdi'] && sell == 0) {
              adx_res_temp2.push('SELL');
              buy = 0;
              sell = 1;
            }
            else {
              adx_res_temp2.push('');
            }
          });
          var starting_index = cp.length - adx_res_temp2.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              adx_final[j] = '';
            }
            adx_final[i] = adx_res_temp2[j];
          }

          indi_ar.push(adx_final);
          break;

        case 'atr':
          tp = indi_params[0]['atr_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var atr_res_temp = this.ti.atr(input);
          var atr_res = [];
          var atr_final = [];
          starting_index = cp.length - atr_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              atr_res[j] = '';
            }
            atr_res[i] = atr_res_temp[j];
          }
          buy = 0;
          sell = 0;
          $.each(atr_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                atr_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                atr_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                atr_final[i] = '';
              }
            }
            else {
              atr_final[i] = '';
            }
          });
          indi_ar.push(atr_final);
          break;

        case 'bollingerbands':
          tp = parseInt(indi_params[0]['bb_time_period']);
          var sd1 = parseInt(indi_params[1]['bb_stddev']);
          input = { period: tp, values: cp, stdDev: sd1 };
          var bollingerbands_res = this.ti.bollingerbands(input);
          var bb_lower = [];
          var bb_upper = [];
          var bollingerbands_final = [];
          var remain = cp.length - bollingerbands_res.length;
          j = remain;
          $.each(bollingerbands_res, function (i, val) {
            if (i < remain) {
              bb_lower[i] = '';
              bb_upper[i] = '';
            }
            bb_lower[j] = val['lower'];
            bb_upper[j] = val['upper'];
            j++;
          });
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (val != '') {
              if (val < bb_lower[i] && buy == 0) {
                bollingerbands_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val > bb_upper[i] && sell == 0) {
                bollingerbands_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                bollingerbands_final[i] = '';
              }
            }
            else {
              bollingerbands_final[i] = '';
            }
          });
          indi_ar.push(bollingerbands_final);
          break;

        case 'cci':
          tp = parseInt(indi_params[0]['cci_time_period']);
          var ul = parseInt(indi_params[1]['cci_upper_limit']);
          var ll = parseInt(indi_params[2]['cci_lower_limit']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp };
          var cci_res_temp = this.ti.cci(input);
          var cci_res = [];
          var cci_final = [];
          starting_index = cp.length - cci_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              cci_res[j] = '';
            }
            cci_res[i] = cci_res_temp[j];
          }
          sell = 0;
          buy = 0;
          $.each(cci_res, function (i, val) {
            if (val != '') {
              if (val >= ul && buy == 0) {
                cci_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val < ll && sell == 0) {
                cci_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                cci_final[i] = '';
              }
            }
            else {
              cci_final[i] = '';
            }
          });

          indi_ar.push(cci_final);
          break;

        case 'forceindex':
          tp = parseInt(indi_params[0]['fi_time_period']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp, volume: volume };
          var forceindex_res_temp = this.ti.forceindex(input);
          var forceindex_final = [];
          starting_index = cp.length - forceindex_res_temp.length;
          var forceindex_res = array_pos_1(starting_index, forceindex_res_temp, cp);
          buy = 0;
          sell = 0;
          $.each(forceindex_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                forceindex_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                forceindex_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                forceindex_final[i] = '';
              }
            }
            else {
              forceindex_final[i] = '';
            }
          });

          indi_ar.push(forceindex_final);
          break;

        case 'kst':
          tp = parseInt(indi_params[0]['kst_time_period']);
          var roc1 = parseInt(indi_params[1]['kst_roc1']);
          var roc2 = parseInt(indi_params[2]['kst_roc2']);
          var roc3 = parseInt(indi_params[3]['kst_roc3']);
          var roc4 = parseInt(indi_params[4]['kst_roc4']);
          var smroc1 = parseInt(indi_params[5]['kst_smroc1']);
          var smroc2 = parseInt(indi_params[6]['kst_smroc2']);
          var smroc3 = parseInt(indi_params[7]['kst_smroc3']);
          var smroc4 = parseInt(indi_params[8]['kst_smroc4']);
          input = {
            values: cp,
            ROCPer1: roc1,
            ROCPer2: roc2,
            ROCPer3: roc3,
            ROCPer4: roc4,
            SMAROCPer1: smroc1,
            SMAROCPer2: smroc2,
            SMAROCPer3: smroc3,
            SMAROCPer4: smroc4,
            signalPeriod: tp
          };
          var kst_res_temp = this.ti.kst(input);
          var kst_res_temp1 = [];
          $.each(kst_res_temp, function (i, val) {
            kst_res_temp1.push(val['kst']);
          });
          starting_index = cp.length - kst_res_temp1.length;
          var kst_res = array_pos_1(starting_index, kst_res_temp1, cp);
          var kst_final = [];
          buy = 0;
          sell = 0;
          $.each(kst_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                kst_final[i] = 'BUY'
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                kst_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                kst_final[i] = '';
              }
            }
            else {
              kst_final[i] = '';
            }
          });
          indi_ar.push(kst_final);
          break;

        case 'macd':
          var fp = parseInt(indi_params[0]['macd_fast_period']);
          var sp = parseInt(indi_params[1]['macd_slow_period']);
          var slp = parseInt(indi_params[2]['macd_signal_period']);
          input = {
            values: cp,
            fastPeriod: fp,
            slowPeriod: sp,
            signalPeriod: slp,
            SimpleMAOscillator: false,
            SimpleMASignal: false
          };
          var macd_res_temp = this.ti.macd(input);
          var macd_res_temp1 = [];
          var macd_final = [];
          $.each(macd_res_temp, function (i, val) {
            if (val['signal'] != undefined) {
              macd_res_temp1[i] = val['signal'];
            }
            else {
              macd_res_temp1[i] = '';
            }
          });
          starting_index = cp.length - macd_res_temp1.length;
          var macd_res = array_pos_1(starting_index, macd_res_temp1, cp);
          buy = 0;
          sell = 0;
          $.each(macd_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                macd_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                macd_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                macd_final[i] = '';
              }
            }
            else {
              macd_final[i] = '';
            }
          });
          indi_ar.push(macd_final);
          break;
        case 'obv':
          input = { close: cp, volume: volume };
          var obv_res_temp = this.ti.obv(input);
          starting_index = cp.length - obv_res_temp.length;
          var obv_res = array_pos_1(starting_index, obv_res_temp, cp);
          buy = 0;
          sell = 0;
          var obv_final = [];
          $.each(obv_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                obv_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                obv_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                obv_final[i] = '';
              }
            }
            else {
              obv_final[i] = '';
            }
          });
          indi_ar.push(obv_final);
          break;
        case 'psar':
          var step = parseInt(indi_params[0]['psar_steps']);
          var max = parseInt(indi_params[1]['psar_max']);
          input = { high: hp, low: lp, step: step, max: max };
          var psar_res_temp = this.ti.psar(input);
          var psar_final = [];
          buy = 0;
          sell = 0;
          $.each(psar_res_temp, function (i, val) {
            if (cp[i] >= val && buy == 0) {
              psar_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (cp[i] < val && sell == 0) {
              psar_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              psar_final[i] = '';
            }
          });
          indi_ar.push(psar_final);
          break;
        case 'roc':
          tp = parseInt(indi_params[0]['roc_time_period']);
          input = { period: tp, values: cp };
          var roc_res_temp = this.ti.roc(input);
          starting_index = cp.length - roc_res_temp.length;
          var roc_res = array_pos_1(starting_index, roc_res_temp, cp);
          var roc_final = [];
          buy = 0;
          sell = 0;
          $.each(roc_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                roc_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                roc_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                roc_final[i] = '';
              }
            }
            else {
              roc_final[i] = '';
            }
          });
          indi_ar.push(roc_final);
          break;
        case 'rsi':
          tp = parseInt(indi_params[0]['rsi_time_period']);
          var ul = parseInt(indi_params[1]['rsi_upper_limit']);
          var ll = parseInt(indi_params[2]['rsi_lower_limit']);
          input = { period: tp, values: cp };
          var rsi_res_temp = this.ti.rsi(input);
          starting_index = cp.length - rsi_res_temp.length;
          var rsi_res = array_pos_1(starting_index, rsi_res_temp, cp);
          var rsi_final = [];
          buy = 0;
          sell = 0;
          $.each(rsi_res, function (i, val) {
            if (val != '') {
              if (val <= ll && buy == 0) {
                rsi_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val >= ul && sell == 0) {
                rsi_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                rsi_final[i] = '';
              }
            }
            else {
              rsi_final[i] = '';
            }
          });
          indi_ar.push(rsi_final);
          break;
        case 'sma':
          var short = parseInt(indi_params[0]['sma_short_period']);
          var input_short = { period: short, values: cp };
          var long = parseInt(indi_params[1]['sma_long_period']);
          var input_long = { period: long, values: cp };
          var sma_short_temp = this.ti.sma(input_short);
          var sma_long_temp = this.ti.sma(input_long);
          var starting_index_short = cp.length - sma_short_temp.length;
          var sma_short_temp1 = array_pos_1(starting_index_short, sma_short_temp, cp);
          var starting_index_long = cp.length - sma_long_temp.length;
          var sma_long_temp1 = array_pos_1(starting_index_long, sma_long_temp, cp);
          var sma_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (sma_short_temp1[i] != '' && sma_long_temp1[i] != '') {
              if (sma_short_temp1[i] >= sma_long_temp1[i] && buy == 0) {
                sma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (sma_short_temp1[i] < sma_long_temp1[i] && sell == 0) {
                sma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                sma_final[i] = '';
              }
            }
            else {
              sma_final[i] = '';
            }
          });
          indi_ar.push(sma_final);
          break;

        case 'ema':
          short = parseInt(indi_params[0]['ema_short_period']);
          input_short = { period: short, values: cp };
          long = parseInt(indi_params[1]['ema_long_period']);
          input_long = { period: long, values: cp };
          var ema_short_temp = this.ti.ema(input_short);
          var ema_long_temp = this.ti.ema(input_long);
          starting_index_short = cp.length - ema_short_temp.length;
          var ema_short_temp1 = array_pos_1(starting_index_short, ema_short_temp, cp);
          starting_index_long = cp.length - ema_long_temp.length;
          var ema_long_temp1 = array_pos_1(starting_index_long, ema_long_temp, cp);
          var ema_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (ema_short_temp1[i] != '' && ema_long_temp1[i] != '') {
              if (ema_short_temp1[i] >= ema_long_temp1[i] && buy == 0) {
                ema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (ema_short_temp1[i] < ema_long_temp1[i] && sell == 0) {
                ema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                ema_final[i] = '';
              }
            }
            else {
              ema_final[i] = '';
            }
          });
          indi_ar.push(ema_final);
          break;

        case 'stochastic':
          tp = parseInt(indi_params[0]['kd_time_period']);
          sp = parseInt(indi_params[1]['kd_signal_period']);
          input = { high: hp, low: lp, close: cp, period: tp, signalPeriod: sp };
          var stochastic_res_temp = this.ti.stochastic(input);
          var stochastic_final = [];
          var stochastic_k = [];
          var stochastic_d = [];
          remain = cp.length - stochastic_res_temp.length;
          j = remain;
          buy = 0;
          sell = 0;
          $.each(stochastic_res_temp, function (i, val) {
            if (val['k'] != undefined && val['d'] != undefined) {
              stochastic_k.push(val['k']);
              stochastic_d.push(val['d']);
            }
            else {
              stochastic_k.push('');
              stochastic_d.push('');
            }
          });
          var starting_index_k = cp.length - stochastic_k.length;
          var stochastic_k1 = array_pos_1(starting_index_k, stochastic_k, cp);
          var starting_index_d = cp.length - stochastic_d.length;
          var stochastic_d1 = array_pos_1(starting_index_d, stochastic_d, cp);
          $.each(cp, function (i, val) {
            if (stochastic_k1[i] != '' && stochastic_d1[i] != '') {
              if (stochastic_d1[i] >= stochastic_k1[i] && buy == 0) {
                stochastic_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (stochastic_d1[i] < stochastic_k1[i] && sell == 0) {
                stochastic_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                stochastic_final[i] = '';
              }
            }
            else {
              stochastic_final[i] = '';
            }
          });
          indi_ar.push(stochastic_final);
          break;

        case 'trix':
          tp = parseInt(indi_params[0]['trix_time_period']);
          input = { values: cp, period: tp };
          var trix_res_temp = this.ti.trix(input);
          starting_index = cp.length - trix_res_temp.length;
          var trix_res = array_pos_1(starting_index, trix_res_temp, cp);
          var trix_final = [];
          buy = 0; sell = 0;
          $.each(trix_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                trix_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                trix_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                trix_final[i] = '';
              }
            }
            else {
              trix_final[i] = '';
            }
          });
          indi_ar.push(trix_final);
          break;

        case 'vwap':
          input = {
            open: op,
            high: hp,
            low: lp,
            close: cp,
            volume: volume
          };
          var vwap_res_temp = this.ti.vwap(input);
          var vwap_final = [];
          buy = 0; sell = 0;
          $.each(vwap_res_temp, function (i, val) {
            if (val >= cp[i] && buy == 0) {
              vwap_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (val < cp[i] && sell == 0) {
              vwap_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              vwap_final[i] = '';
            }
          });
          indi_ar.push(vwap_final);
          break;

        case 'wma':
          tp = parseInt(indi_params[0]['wma_time_period']);
          input = { values: cp, period: tp };
          var wma_res_temp = this.ti.wma(input);
          starting_index = cp.length - wma_res_temp.length;
          var wma_res = array_pos_1(starting_index, wma_res_temp, cp);
          var wma_final = [];
          buy = 0; sell = 0;
          $.each(wma_res, function (i, val) {
            if (val != '') {
              if (val >= cp[i] && buy == 0) {
                wma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < cp[i] && sell == 0) {
                wma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wma_final[i] = '';
              }
            }
            else {
              wma_final[i] = '';
            }
          });
          indi_ar.push(wma_final);
          break;

        case 'wema':
          tp = parseInt(indi_params[0]['wema_time_period']);
          input = { values: cp, period: tp };
          var wema_res_temp = this.ti.wema(input);
          starting_index = cp.length - wema_res_temp.length;
          var wema_res = array_pos_1(starting_index, wema_res_temp, cp);
          var wema_final = [];
          buy = 0; sell = 0;
          $.each(wema_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                wema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                wema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wema_final[i] = '';
              }
            }
            else {
              wema_final[i] = '';
            }
          });
          indi_ar.push(wema_final);
          break;

        case 'williamsr':
          tp = parseInt(indi_params[0]['williamsr_time_period']);
          input = { high: hp, low: lp, close: cp, period: tp };
          var williamsr_res_temp = this.ti.williamsr(input);
          starting_index = cp.length - williamsr_res_temp.length;
          var williamsr_res = array_pos_1(starting_index, williamsr_res_temp, cp);
          var williamsr_final = [];
          buy = 0; sell = 0;
          $.each(williamsr_res, function (i, val) {
            if (val != '') {
              if (val >= 20 && buy == 0) {
                williamsr_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 20 && sell == 0) {
                williamsr_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                williamsr_final[i] = '';
              }
            }
            else {
              williamsr_final[i] = '';
            }
          });
          indi_ar.push(williamsr_final);
          break;

        default:
          break;
      }
      indi_name_ar.push(indi + '_final');
    });

    let sl_ar = [];
    $.each(cp, (i, val) => {
      if (this.lastsl > val && this.slstate == 1) {
        sl_ar[i] = 'SELL';
        this.tpstate = 0;
        this.slstate = 0;
      }
      else {
        sl_ar[i] = '';
      }
    });
    indi_ar.push(sl_ar);

    let tp_ar = [];
    $.each(cp, (i, val) => {
      if (this.lasttp < val && this.tpstate == 1) {
        tp_ar[i] = 'SELL';
        this.tpstate = 0;
        this.slstate = 0;
      }
      else {
        tp_ar[i] = '';
      }
    });
    indi_ar.push(tp_ar);
    var hold_key_ar = [];
    var buy_key_ar = [];
    var sell_key_ar = [];
    $.each(cp, function (i, val) {
      buy_key_ar[i] = 0;
      sell_key_ar[i] = 0;
      hold_key_ar[i] = 0;
    });
    $.each(indi_ar, function (i, val) {
      $.each(val, function (j, val1) {
        if (val1 == 'SELL') {
          sell_key_ar[j] += 1;
        }
        else if (val1 == 'BUY') {
          buy_key_ar[j] += 1;
        }
        else {
          hold_key_ar[j] += 1;
        }
      });
    });
    let buy_sell = [];
    buy = 0; sell = 0;
    $.each(cp, (i, val) => {
      if (sell_key_ar[i] > buy_key_ar[i] && sell == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'BUY';
        }
        else {
          buy_sell[i] = 'SELL';
        }
      }
      else if (sell_key_ar[i] < buy_key_ar[i] && buy == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'SELL';
        }
        else {
          buy_sell[i] = 'BUY';
        }
      }
      else {
        buy_sell[i] = '';
      }
    });
    if (cp.length > 0) {
      $.each(buy_sell, (i, val) => {
        if (i == (buy_sell.length - 1)) {
          if (buy_sell.length - this.final_buy_sell.length == 1) {
            this.final_buy_sell[i] = val;
          }
          else {
            this.final_buy_sell[i] = val;
          }
        }
      });
      this.final_buy_sell = this.ReArrange(this.final_buy_sell);
      this.MakeChart(timestamp, cp, stop_loss, take_profit, portfolio);
    }
  }
  BotTypeChange(){
    if (this.autonio_token != "" && this.autonio_token != undefined) {
      $.each(this.autonio_token.keys, (i, val) => {
        if (val.name != "bitshares") {
          if (this.botType == "normal") {
            if (
              val.api != "" &&
              val.secret != "" &&
              val.name != "binance" &&
              val.name != "kucoin" &&
              val.name != "cryptopia" &&
              val.name != "ethfinex" &&
              val.name != "nebula"
            ) {
              this.exchange_ar.push(val.name);
            }
          } else {
            if (
              val.api != "" &&
              val.secret != "" &&
              val.name != "bittrex" &&
              val.name != "bitfinex" &&
              val.name != "kraken" &&
              val.name != "bitstamp" &&
              val.name != "quadrigax"
            ) {
              this.exchange_ar.push(val.name);
            }
          }
        }
      });
    }
  }
  randomIntFromInterval(min, max) {
    max = parseInt(max);
    min = parseInt(min);
    console.log(max, min);
    if (max - min >= 2) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    } else {
      return min;
    }
  }
  async updateAssetBalance() {
    let fetchFreeBalance;
    let fetchBalance;
    console.log(this.exc_s);
    var scope=this;
    if (this.exc_s == "nebula") {
      fetchFreeBalance = await this.ccxt_current.fetch_free_balance({
        recvWindow: 10000000
      });
    } else {
      fetchBalance = await this.ccxt_current.fetchBalance({
        recvWindow: 10000000
      });
      fetchFreeBalance = fetchBalance.free;
    }
    if (!(this.asset in fetchFreeBalance)) {
      this.electron_er("couldnot find"+ this.asset + "in your wallet");
      this.StopMarketMaker();
      return;
    } else {
      $.each(fetchFreeBalance, (i, val) => {
        if (i === this.asset) {
          this.amount_to_use = this.PerCalc(val, this.portfolio_mm_s);
          this.asst_in_wallet=val;
        }
      });
    }
  }
  addToStat_msg(sig,pair,qty,prc,time){
    let trade={
      signal:sig,
      symbol:pair,
      amount:qty,
      bid:prc,
      time:mnt(time).format('YYYY-MM-DD HH:mm:ss')
    };
    this.market_stat_msg.unshift(trade);
    if (this.market_stat_msg.length > 5 && this.botType == "marketmaker") {
      this.market_stat_msg.pop();
    }
  }
  addToStat(msg, cls) {
    let obj = {
      txt: msg,
      class: cls,
      time: mnt(new Date().toString()).format("YYYY/MM/DD HH:mm:ss")
    };
    this.order_stat_msg.unshift(obj);
    if (this.order_stat_msg.length > 5 && this.botType == "marketmaker") {
      this.order_stat_msg.pop();
    }
  }
  updateBaseBalance() {
    (async () => {
      let fetchBalance = await this.ccxt_current.fetchBalance({
        recvWindow: 10000000
      });
      let fetchFreeBalance = fetchBalance.free;
      if (!(this.base in fetchFreeBalance)) {
        this.electron_er("couldnot find "+this.base+ "in your wallet");
        return;
      } else {
        $.each(fetchBalance.free, (i, val) => {
          if (i === this.base) {
            this.base_balance = val;
            this.base_val=val;
          }
        });
      }
    })();
  }
  async checkAssetBalance() {
    let fetchFreeBalance;
    let fetchBalance;
    var scope=this;
    if (this.exc_s == "nebula") {
      fetchFreeBalance = await this.ccxt_current.fetch_free_balance({
        recvWindow: 10000000
      });
    } else {
      fetchBalance = await this.ccxt_current.fetchBalance({
        recvWindow: 10000000
      });
      fetchFreeBalance = fetchBalance.free;
    }
    if (!(this.asset in fetchFreeBalance)) {
      this.electron_er("couldnot find"+ this.asset + "in your wallet");
      this.StopMarketMaker();
      return;
    } else {
      $.each(fetchFreeBalance, (i, val) => {
        if (i === this.asset) {
          this.start_maker_bal_asset=val;
          cc.price(i, 'USD')
          .then(prices => {
            scope.asst_in_wallet=val;
            scope.asset_val=val*prices.USD;
            scope.strt_asst=prices.USD;
            scope.start_maker_bal=parseFloat((scope.base_val+scope.asset_val).toFixed(8));
          })
          .catch(console.error)
        }
        if(i === this.base){
          this.start_maker_bal_base=val;
          if(this.base!= 'USD'){
            cc.price(i, 'USD')
            .then(prices => {
              scope.base_val=val*prices.USD;
              scope.start_maker_bal=scope.base_val+scope.asset_val;
            })
            .catch(console.error)
          }
          else{
              scope.base_val=val;
          }
        }
      });
    }
  }
  checkAssetBalanceAtEnd(){
    cc.price(this.asset, 'USD')
    .then(prices => {
      this.end_asst=prices.USD;
    });
  }
MarketMaker() {
  this.form_dat=0;
  this.strt_date=0;
  let d = new Date();
  this.strt_date=d.getTime();
  this.market_stat_msg = [];
  this.order_stat_msg=[];
  this.fillled_order=[];
  this.start_maker_bal=0;
  this.end_maker_bal=0;
  this.maker_profit_loss=0;
  this.maker_profit_loss_buy_hold=0;
  this.buyvol=0;
  this.sellvol=0;
  this.noof_tran=0;
  this.checkInterval=0;

  var market_maker_bot_data = $('form').serializeArray();
  this.form_dat=market_maker_bot_data;
  if (this.pair_s == undefined || this.main_s_bot == undefined) {
    this.electron_er("Missing Fields, Please check all fields properly");
  } else if (this.portfolio_mm_s <= 0) {
    this.electron_er("Please Check Portfolio Percentage");
  } else {
    let asset = this.asset;
    let base = this.base;
    let spread: number = Number(this.spread_s);
    let spread_s: number = Number(this.spread_s_s);
    let exc = this.exc_s;
    let pair = this.pair_s;
    $.each(this.autonio_token.keys, (i, val) => {
      if (val.name == exc) {
        if(exc != 'bitshares' && exc != 'GDEX'&& exc != 'CryptoBridge' && exc != 'EasyDex'){
          this.apiCurrent = val.api;
          this.secretCurrent = val.secret;
          if(val.password){
            this.password=val.password;
          }
        }
        else{
          if (val.account != '' && val.api != '') {
            if(exc=='bitshares' || exc== 'GDEX' || exc== 'CryptoBridge' || exc== 'EasyDex'){
              this.account = val.account;
              this.api = val.api;
            }
          }
        }
      }
    });
    if(exc != 'bitshares' && exc != 'GDEX'&& exc != 'CryptoBridge' && exc != 'EasyDex'){
      this.ccxt_current = new ccxt[exc];
      // //for build it doesnot require...
      // if (!this.ccxt_current.has["CORS"] && exc != "nebula") {
      //   this.ccxt_current.proxy = this.proxy;
      // }
      this.ccxt_current.apiKey = this.apiCurrent;
      this.ccxt_current.secret = this.secretCurrent;
      this.ccxt_current.timeout = this.timeout;
      if(exc=='kucoin'||exc=='kucoin2'  ){
      this.ccxt_current.password=this.password;
      }
      (async () => {
        try {
          await this.checkAssetBalance();
          $('.result_preloader').fadeOut(200);
          let orderid = "458b39c5-ddd3-4763-b7c1-f05f0dd9ac6c";
          let orderid2 = "5897f1d9-5ba9-4f66-95d7-621c6d188485";
          let orderid3 = "6a97c5c1-a465-4ea5-a228-394220648a9d";
          let refresh = this.refresh_s;
          let bot_type = this.main_s_bot;
          // let livemode = this.livemode_s;
          this.last_buy_id = undefined;
          this.last_sell_id = undefined;
          this.isBuy = true;
          this.isSell = true;
          if (this.select_portfolio_type == "random") {
            this.portfolio_mm_s = this.randomIntFromInterval(
              this.portfolio_mm_s_from,
              this.portfolio_mm_s_to
            );
          }
          await this.updateAssetBalance();
          let asset_bal = this.amount_to_use;
          let base_bal = this.amount_to_use;
          this.base_balance = base_bal;
          this.asset_balance = asset_bal;
          $('.page-body #marketmaker .overlay').fadeIn(200);
          if(exc=='okex'){
            this.runMarketMakerOkex(bot_type, asset, base, pair, spread, spread_s, asset_bal, base_bal);
            this.marketmakerInterval = setInterval(async () => {
              this.runMarketMakerOkex(bot_type, asset, base, pair, spread, spread_s, asset_bal, base_bal);
              if(this.checkInterval==1){
                clearInterval(this.marketmakerInterval);
              }
            }, refresh * 1000);
          }else{
            this.runMarketMaker(bot_type, asset, base, pair, spread, spread_s, asset_bal, base_bal);
            this.marketmakerInterval = setInterval(async () => {
              this.runMarketMaker(bot_type, asset, base, pair, spread, spread_s, asset_bal, base_bal);
              if(this.checkInterval==1){
                clearInterval(this.marketmakerInterval);
              }
            }, refresh * 1000);
          }
        } catch (e) {
          this.electron_er(e+"error from 327 line run mkt maker");
          this.StopMarketMaker();
        } finally {
        }
      })();
    }
    else{
      let refresh = this.refresh_s;
      let bot_type = this.main_s_bot;
      this.last_buy_id = undefined;
      this.last_sell_id = undefined;
      this.isBuy = true;
      this.isSell = true;
      if (this.select_portfolio_type == "random") {
        this.portfolio_mm_s = this.randomIntFromInterval(
          this.portfolio_mm_s_from,
          this.portfolio_mm_s_to
        );
      }
      BitShares.subscribe('connected',
      async () => {
        let asset_ = await BitShares.db.list_assets(asset, 1);
        let base_ = await BitShares.db.list_assets(base, 1);
        this.asset_precision = asset_[0]['precision'];
        this.base_precision = base_[0]['precision'];
        let bot = new BitShares(this.account, this.api);
        let balance = await bot.balances();
        console.log("bitshares se balance information==",balance);
        await $.each(balance, (i, val) => {
          if (val['asset']['symbol'] == asset || val['asset']['symbol'] == base) {
              let precision = val['asset']['precision'];
              let amount = val['amount'] / (10 ** precision);
              this.start_maker_bal=amount;
              this.amount_to_use=this.PerCalc(amount,this.portfolio_mm_s);
          }
        });
      }
    )
    let asset_bal = this.amount_to_use;
    let base_bal = this.amount_to_use;
    this.start_maker_bal_asset=this.start_maker_bal;
    this.start_maker_bal_base=this.start_maker_bal;
    this.runMarketMaker1(bot_type, asset, base, spread, asset_bal, base_bal);
    this.marketmakerInterval = setInterval(() => {
      this.runMarketMaker1(bot_type, asset, base, spread, asset_bal, base_bal);
      if(this.checkInterval==1){
        clearInterval(this.marketmakerInterval);
      }
    }, refresh * 1000);
    }
  }
}
runMarketMaker1(bot_type, asset, base, spread: number, asset_bal, base_bal) {
  BitShares.subscribe('connected',
    async () => {

      let ticker = await BitShares.db.get_ticker(base, asset);
      let bid: number = parseFloat((ticker['highest_bid']));
      let ask: number = parseFloat((ticker['lowest_ask']));
      let middle: number = parseFloat(((bid + ask) / 2).toFixed(5));
      let buy_price: number = parseFloat((middle - this.PerCalc(bid, (spread / 2))).toFixed(5));
      let sell_price: number = parseFloat((middle + this.PerCalc(bid, (spread / 2))).toFixed(5));

      try {
        let bot = new BitShares(this.account, this.api);
        if (this.last_buy_id != undefined) {
          let orderStat = await bot.getOrder(this.last_buy_id);
          if (orderStat != null) {
            this.isBuyFilled = false;
            if (bot_type == 'o_3') {
              this.base_balance = orderStat['for_sale'] / (10 ** this.base_precision);
            }
            else {
              this.base_balance = base_bal;
            }
            await bot.cancelOrder(this.last_buy_id);
            let msg = 'Last BUY Order Canceled';
            this.addToStat(msg, 'green');
          }
          else {
            let count=0;
            if(this.fillled_order.length>0){
              $.each(this.fillled_order, (i, val1) => {
                if (val1["id"] == orderStat["id"]) {
                  count+=1;
                }
              });
            }
            if(count==0){
              this.fillled_order.unshift(orderStat);

            }
            this.isBuyFilled = true;
            this.isBuy = false;
          }
        }
        if (this.last_sell_id != undefined) {
          let orderStat = await bot.getOrder(this.last_sell_id);
          if (orderStat != null) {
            this.isSellFilled = false;
            if (bot_type == 'o_3') {
              this.asset_balance = orderStat['for_sale'] / (10 ** this.asset_precision);
            }
            else {
              this.asset_balance = asset_bal;
            }
            await bot.cancelOrder(this.last_sell_id);
            let msg = 'Last SELL Order Canceled';
            this.addToStat(msg, 'red');
          }
          else {
            let count=0;
            if(this.fillled_order.length>0){
              $.each(this.fillled_order, (i, val1) => {
                if (val1["id"] == orderStat["id"]) {
                  count+=1;
                }
              });
            }
            if(count==0){
              this.fillled_order.unshift(orderStat);

            }
            this.isSellFilled = true;
            this.isSell = false;
          }
        }
        if (parseFloat(this.asset_balance) < 0.001) {
          this.isSellFilled = true;
          this.isSell = false;
        }
        else if (parseFloat((this.base_balance / buy_price).toFixed(5)) < 0.001) {
          this.isBuyFilled = true;
          this.isBuy = false;
        }
        if (this.isBuyFilled && this.isSellFilled) {
          this.isBuy = true;
          this.isSell = true;
          this.base_balance = base_bal;
          this.asset_balance = asset_bal;
        }
        let buy_quantity: number = parseFloat((this.base_balance / buy_price).toFixed(5));
        let sell_quantity: number = parseFloat((this.asset_balance * 1).toFixed(5));

        let balance = await bot.balances();
        let balance_available = true;
        await $.each(balance, (i, val) => {
          if (val['asset']['symbol'] == asset || val['asset']['symbol'] == base) {
            let precision = val['asset']['precision'];
            let amount = val['amount'] / (10 ** precision);
            if (val['asset']['symbol'] == asset && amount < this.asset_balance) {
              this.electron_er("You don't have sufficient fund of " + asset);
              balance_available = false;
              this.StopMarketMaker();
            }
            if (val['asset']['symbol'] == base && amount < this.base_balance) {
              this.electron_er("You don't have sufficient fund of " + base);
              balance_available = false;
              this.StopMarketMaker();
            }
          }
        });

        if (balance_available) {
          console.log(this.isSell, this.isBuy);
          if (bot_type == 'o_1') {
            this.isBuy = true;
            this.isSell = true;
          }
          // BUY
          if (this.isBuy) {
            let buy_order = await bot.buy(asset, base, buy_quantity, buy_price);
            console.log("placed buy order==", buy_order);
            let buy_msg = 'Place BUY order For ' + buy_quantity + ' ' + asset + ' With Price ' + buy_price;
            this.addToStat_msg("BUY",this.pair_s,buy_quantity,buy_price,buy_order.timestamp);
            if (buy_order != null) {
              this.last_buy_id = buy_order['id'];
              this.isBuyFilled = false;
            }
            else {
              let count=0;
              if(this.fillled_order.length>0){
                $.each(this.fillled_order, (i, val1) => {
                  if (val1["id"] == buy_order["id"]) {
                    count+=1;
                  }
                });
              }
              if(count==0){
                this.fillled_order.unshift(buy_order);
              }
              this.last_buy_id = undefined;
              this.isBuyFilled = true;
              this.isBuy = false;
            }
          }
          // SELL
          if (this.isSell) {
            let sell_order = await bot.sell(asset, base, sell_quantity, sell_price);
            console.log("Placed sell order==",sell_order);
            let sell_msg = 'Place SELL order For ' + sell_quantity + ' ' + asset + ' With Price ' + sell_price;
            this.addToStat_msg("SELL",this.pair_s,sell_quantity,sell_price,sell_order.timestamp);
            if (sell_order != null) {
              this.last_sell_id = sell_order['id'];
              this.isSellFilled = false;
            }
            else {
              let count=0;
              if(this.fillled_order.length>0){
                $.each(this.fillled_order, (i, val1) => {
                  if (val1["id"] == sell_order["id"]) {
                    count+=1;
                  }
                });
              }
              if(count==0){
                console.log('Sell Filled===',sell_order);
                this.fillled_order.unshift(sell_order);
              }
              this.last_sell_id = undefined;
              this.isSellFilled = true;
              this.isSell = false;
            }
          }

          if (this.isBuyFilled && this.isSellFilled) {
            this.isBuy = true;
            this.isSell = true;
            this.base_balance = base_bal;
            this.asset_balance = asset_bal;
          }
        }
      }
      catch (e) {
        this.electron_er(e+" line no 3314");
        this.StopMarketMaker();
      }

    }
  );
}
async runMarketMakerOkex(
bot_type,
asset,
base,
pair,
spread: number,
spread_s: number,
asset_bal,
base_bal
) {
let buyvol_data:number=0;
let sellvol_data:number=0;
this.chart = null;
let chartData='[';
let ticker = await this.ccxt_current.fetchTicker(pair);
let bid: number = parseFloat(ticker["bid"]);
let ask: number = parseFloat(ticker["ask"]);
let middle: number = parseFloat(((bid + ask) / 2).toFixed(8));
let buy_price: number;
let sell_price: number;
if (this.select_spread == "s_s") {
  buy_price = parseFloat(((middle - spread_s) * 1).toFixed(8));
  sell_price = parseFloat(((middle + spread_s) * 1).toFixed(8));
} else if (this.select_spread == "s") {
  buy_price = parseFloat(
    (middle - this.PerCalc(bid, spread / 2)).toFixed(8)
  );
  sell_price = parseFloat(
    (middle + this.PerCalc(bid, spread / 2)).toFixed(8)
  );
}
try {
  this.ccxt_current.enableRateLimit = true;

  if (this.last_sell_id != undefined) {
    let orderStat = await this.ccxt_current.fetchOrder(
      this.last_sell_id,
      pair,
    );
    if (orderStat["remaining"] > 0) {
      this.isSellFilled = false;
      await this.ccxt_current.cancelOrder(
        this.last_sell_id,
         pair,
       );
      let msg = "Last SELL Order Canceled";
      this.addToStat(msg, "red");
      if (bot_type == "o_3") {
        this.asset_balance = orderStat["remaining"];
      } else {
        await this.updateAssetBalance();
        this.asset_balance = this.amount_to_use;
      }
    } else {
      let count=0;
      let percent=0.0;
      percent = parseFloat(this.portfolio_mm_s)/100;
      if(this.fillled_order.length>0){
        $.each(this.fillled_order, (i, val1) => {
          if (val1["id"] == orderStat["id"]) {
            count+=1;
          }
        });
      }
      if(count==0){
        this.fillled_order.unshift(orderStat);
        sellvol_data=orderStat["filled"]*orderStat["price"];
        this.sellvol+=sellvol_data;
        this.noof_tran+=1;
        buyvol_data=0;
        this.checkBalanceAtEnd();
      }
      this.isSellFilled = true;
      this.isSell = false;
    }
  }

  if (this.last_buy_id != undefined) {
    let orderStat = await this.ccxt_current.fetchOrder(
      this.last_buy_id,
      pair,
    );
    if (orderStat["remaining"] > 0) {
      this.isBuyFilled = false;
      await this.ccxt_current.cancelOrder(
        this.last_buy_id,
         pair,
       );
      let msg = "Last BUY Order Canceled";
      this.addToStat(msg, "green");
      if (bot_type == "o_3") {
        this.base_balance = orderStat["remaining"];
      } else {
        await this.updateAssetBalance();
        this.base_balance = this.amount_to_use;
      }
    } else {
      let count=0;
      let percent=0.0;
      percent =parseFloat(this.portfolio_mm_s)/100;
      if(this.fillled_order.length>0){
        $.each(this.fillled_order, (i, val1) => {
          if (val1["id"] == orderStat["id"]) {
            count+=1;
          }
        });
      }
      if(count==0){
        this.fillled_order.unshift(orderStat);
        buyvol_data=orderStat["filled"]*orderStat["price"];
        this.buyvol+=buyvol_data;
        this.noof_tran+=1;
        buyvol_data=0;
        this.checkBalanceAtEnd();
      }
      this.isBuyFilled = true;
      this.isBuy = false;
    }
  }
  if (this.isBuyFilled && this.isSellFilled) {
    this.isBuy = true;
    this.isSell = true;
    this.base_balance = base_bal;
    this.asset_balance = asset_bal;
    if (this.select_portfolio_type == "random") {
      this.portfolio_mm_s = this.randomIntFromInterval(
        this.portfolio_mm_s_from,
        this.portfolio_mm_s_to
      );
      await this.updateAssetBalance();
      this.base_balance = this.amount_to_use;
      this.asset_balance = this.amount_to_use;
    }
  }

  let buy_quantity: number = parseFloat((this.base_balance * 1).toFixed(8));
  let sell_quantity: number = parseFloat(
    (this.asset_balance * 1).toFixed(8)
  );
  let balance_available = true;
  if (balance_available) {
    console.log(this.isSell, this.isBuy);
    if (bot_type == "o_1") {
      this.isBuy = true;
      this.isSell = true;
    }
    this.end_maker_bal=parseFloat(((this.asst_in_wallet*this.end_asst)+this.base_val).toFixed(8));
    this.maker_profit_loss=parseFloat((parseFloat(this.end_maker_bal)-parseFloat(this.start_maker_bal)).toFixed(8));
    // SELL
    if (this.isSell) {
      let sell_order = await this.ccxt_current.create_limit_sell_order(
        pair,
        sell_quantity,
        sell_price,
        // { recvWindow: 10000000 }
      );
      this.checkAssetBalanceAtEnd();
      console.log(sell_order);
      let sell_msg =
        "Place SELL order For " +
        sell_quantity +
        " " +
        asset +
        " With Price " +
        sell_price;
      chartData += '{"date" : "' + this.formatDate(sell_order.timestamp) + '", "value" : ' + sell_price + ', "customBullet" : "assets/svg/sell.svg"}';
      chartData += ',';
      this.addToStat_msg("SELL",pair,sell_quantity,sell_price,sell_order.timestamp);
      if (sell_order != null) {
        if (this.exc_s == "cryptopia" && sell_order["id"] == undefined) {
          console.log("Sell Filled");
          this.last_sell_id = undefined;
          this.isSellFilled = true;
          this.isSell = false;
        } else {
          this.last_sell_id = sell_order["id"];
          console.log(this.last_sell_id);
          this.isSellFilled = false;
          let order_state = await this.ccxt_current.fetchOrder(
            this.last_sell_id,
            pair,
            // { recvWindow: 10000000 }
          );
          if (order_state["remaining"] == 0) {
            let count=0;
            let percent=0.0;
            percent =parseFloat(this.portfolio_mm_s)/100;
            if(this.fillled_order.length>0){
              $.each(this.fillled_order, (i, val1) => {
                if (val1["id"] == order_state["id"]) {
                  count+=1;
                }
              });
            }
            if(count==0){
              this.fillled_order.unshift(order_state);
              sellvol_data=order_state["filled"]*order_state["price"];
              this.sellvol+=sellvol_data;
              this.noof_tran+=1;
              sellvol_data=0;
              this.checkBalanceAtEnd();
            }
            this.last_sell_id = undefined;
            this.isSellFilled = true;
            this.isSell = false;
          }
        }
      }
    }

    // BUY
    if (this.isBuy) {
      console.log(buy_quantity, buy_price);
      let buy_order = await this.ccxt_current.create_limit_buy_order(
        pair,
        buy_quantity,
        buy_price,
        // { recvWindow: 10000000 }
      );
      this.checkAssetBalanceAtEnd();
      console.log(buy_order);
      chartData += '{"date" : "' + this.formatDate(buy_order.timestamp) + '", "value" : ' + buy_price + ', "customBullet" : "assets/svg/buy.svg"}';
      this.addToStat_msg("BUY",pair,buy_quantity,buy_price,buy_order.timestamp);
      if (buy_order != null) {
        if (this.exc_s == "cryptopia" && buy_order["id"] == undefined) {
          console.log("Buy Filled");
          this.last_buy_id = undefined;
          this.isBuyFilled = true;
          this.isBuy = false;
        } else {
          this.last_buy_id = buy_order["id"];
          console.log(this.last_buy_id);
          this.isBuyFilled = false;
          let order_state = await this.ccxt_current.fetchOrder(
            this.last_buy_id,
            pair,
            // { recvWindow: 10000000 }
          );
          if (order_state["remaining"] == 0) {
            let count=0;
            let percent=0.0;
            percent = parseFloat(this.portfolio_mm_s)/100;
            if(this.fillled_order.length>0){
              $.each(this.fillled_order, (i, val1) => {
                if (val1["id"] == order_state["id"]) {
                  count+=1;
                  console.log("this information already pushed",order_state["id"]);
                }
              });
            }
            if(count==0){
              this.fillled_order.unshift(order_state);
              buyvol_data=order_state["filled"]*order_state["price"];
              this.buyvol+=buyvol_data;
              this.noof_tran+=1;
              buyvol_data=0;
              this.checkBalanceAtEnd();
            }
            console.log("Buy Filled");
            this.last_buy_id = undefined;
            this.isBuyFilled = true;
            this.isBuy = false;
          }
        }
      }
    }
    if (this.isBuyFilled && this.isSellFilled) {
      this.isBuy = true;
      this.isSell = true;
      this.base_balance = base_bal;
      this.asset_balance = asset_bal;
      if (this.select_portfolio_type == "random") {
        this.portfolio_mm_s = this.randomIntFromInterval(
          this.portfolio_mm_s_from,
          this.portfolio_mm_s_to
        );
        await this.updateAssetBalance();
        this.base_balance = this.amount_to_use;
        this.asset_balance = this.amount_to_use;
      }
    }
  }
} catch (e) {
  this.electron_er(e);
  this.StopMarketMaker();
}
  chartData += ']';
  console.log(chartData);
  this.chartData = (JSON.parse(chartData));
  console.log(this.chartData);
  $('#chart').css('height', '100%');
  let chart_config = {
    "type": "serial",
    "theme": "dark",
    "addClassNames": true,
    "dataProvider": this.chartData,
    "dataDateFormat": "YYYY-MM-DD HH:NN",
    "categoryField": "date",
    "creditsPosition": "bottom-right",
    "valueAxes": [{
      "axisAlpha": 0.2,
      "dashLength": 1,
      "position": "left"
    }],
    "defs": {
      "filter": {
        "id": "dropshadow",
        "x": "-10%",
        "y": "-10%",
        "width": "120%",
        "height": "120%",
        "feOffset": {
          "result": "offOut",
          "in": "rgba",
          "dx": "3",
          "dy": "3"
        },
        "feGaussianBlur": {
          "result": "blurOut",
          "in": "offOut",
          "stdDeviation": "5"
        },
        "feBlend": {
          "in": "SourceGraphic",
          "in2": "blurOut",
          "mode": "normal"
        }
      }
    },
    "graphs": [{
      "id": "g1",
      "type": "smoothedLine",
      "bulletSize": 28,
      // "customBullet": '',
      "customBulletField": "customBullet",
      "balloonText": "[[value]]",
      "lineColor": "var(--blue-2)",
      "bulletBorderAlpha": 1,
      "bulletColor": "#FFFFFF",
      "hideBulletsCount": 0,
      "valueField": "value",
      "useLineColorForBulletBorder": true,
      "lineThickness": 2
      // "balloon":{
      //     "drop":true
      // }
    }],
    "chartCursor": {
      "categoryBalloonDateFormat": "HH:NN, DD MMMM",
      "cursorPosition": "mouse"
    },
    "chartScrollbar": {
      "autoGridCount": true,
      "graph": "g1",
      "scrollbarHeight": 40
    },
    "categoryAxis": {
      "minPeriod": "mm",
      "parseDates": true,
      "axisColor": "#DADADA",
      "dashLength": 1,
      "minorGridEnabled": true
    }
  };

  this.chart = this.AmCharts.makeChart("chart", chart_config);
  $('.chart_preloader').fadeOut(200);
}
async runMarketMaker(
  bot_type,
  asset,
  base,
  pair,
  spread: number,
  spread_s: number,
  asset_bal,
  base_bal
) {
  let buyvol_data:number=0;
  let sellvol_data:number=0;
  this.chart = null;
  let chartData='[';
  let ticker = await this.ccxt_current.fetchTicker(pair);
  let bid: number = parseFloat(ticker["bid"]);
  let ask: number = parseFloat(ticker["ask"]);
  let middle: number = parseFloat(((bid + ask) / 2).toFixed(8));
  let buy_price: number;
  let sell_price: number;
  if (this.select_spread == "s_s") {
    buy_price = parseFloat(((middle - spread_s) * 1).toFixed(8));
    sell_price = parseFloat(((middle + spread_s) * 1).toFixed(8));
  } else if (this.select_spread == "s") {
    buy_price = parseFloat(
      (middle - this.PerCalc(bid, spread / 2)).toFixed(8)
    );
    sell_price = parseFloat(
      (middle + this.PerCalc(bid, spread / 2)).toFixed(8)
    );
  }
  try {
    this.ccxt_current.enableRateLimit = true;

    if (this.last_sell_id != undefined) {
      let orderStat = await this.ccxt_current.fetchOrder(
        this.last_sell_id,
        pair,
        { recvWindow: 10000000 }
      );
      if (orderStat["remaining"] > 0) {
        this.isSellFilled = false;
        await this.ccxt_current.cancelOrder(
          this.last_sell_id,
           pair,
           {recvWindow: 10000000}
         );
        let msg = "Last SELL Order Canceled";
        this.addToStat(msg, "red");
        if (bot_type == "o_3") {
          this.asset_balance = orderStat["remaining"];
        } else {
          await this.updateAssetBalance();
          this.asset_balance = this.amount_to_use;
        }
      } else {
        let count=0;
        let percent=0.0;
        percent = parseFloat(this.portfolio_mm_s)/100;
        if(this.fillled_order.length>0){
          $.each(this.fillled_order, (i, val1) => {
            if (val1["id"] == orderStat["id"]) {
              count+=1;
            }
          });
        }
        if(count==0){
          this.fillled_order.unshift(orderStat);
          sellvol_data=orderStat["filled"]*orderStat["price"];
          this.sellvol+=sellvol_data;
          this.noof_tran+=1;
          sellvol_data=0;
          this.checkBalanceAtEnd();
        }
        this.isSellFilled = true;
        this.isSell = false;
      }
    }

    if (this.last_buy_id != undefined) {
      let orderStat = await this.ccxt_current.fetchOrder(
        this.last_buy_id,
        pair,
        { recvWindow: 10000000 }
      );
      if (orderStat["remaining"] > 0) {
        this.isBuyFilled = false;
        await this.ccxt_current.cancelOrder(
          this.last_buy_id,
           pair,
           {recvWindow: 10000000 }
         );
        let msg = "Last BUY Order Canceled";
        this.addToStat(msg, "green");
        if (bot_type == "o_3") {
          this.base_balance = orderStat["remaining"];
        } else {
          await this.updateAssetBalance();
          this.base_balance = this.amount_to_use;
        }
      } else {
        let count=0;
        let percent=0.0;
        percent =parseFloat(this.portfolio_mm_s)/100;
        if(this.fillled_order.length>0){
          $.each(this.fillled_order, (i, val1) => {
            if (val1["id"] == orderStat["id"]) {
              count+=1;
            }
          });
        }
        if(count==0){
          this.fillled_order.unshift(orderStat);
          buyvol_data=orderStat["filled"]*orderStat["price"];
          this.buyvol+=buyvol_data;
          this.noof_tran+=1;
          buyvol_data=0;
          this.checkBalanceAtEnd();
        }
        console.log("Buy Filled");

        this.isBuyFilled = true;
        this.isBuy = false;
      }
    }
    if (this.isBuyFilled && this.isSellFilled) {
      this.isBuy = true;
      this.isSell = true;
      this.base_balance = base_bal;
      this.asset_balance = asset_bal;
      if (this.select_portfolio_type == "random") {
        this.portfolio_mm_s = this.randomIntFromInterval(
          this.portfolio_mm_s_from,
          this.portfolio_mm_s_to
        );
        await this.updateAssetBalance();
        this.base_balance = this.amount_to_use;
        this.asset_balance = this.amount_to_use;
      }
    }

    let buy_quantity: number = parseFloat((this.base_balance * 1).toFixed(8));
    let sell_quantity: number = parseFloat(
      (this.asset_balance * 1).toFixed(8)
    );

    let balance_available = true;
    if (balance_available) {
      console.log(this.isSell, this.isBuy);
      if (bot_type == "o_1") {
        this.isBuy = true;
        this.isSell = true;
      }
      this.end_maker_bal=parseFloat(((this.asst_in_wallet*this.end_asst)+this.base_val).toFixed(8));
      this.maker_profit_loss=parseFloat((parseFloat(this.end_maker_bal)-parseFloat(this.start_maker_bal)).toFixed(8));
      // SELL
      if (this.isSell) {
        let sell_order = await this.ccxt_current.create_limit_sell_order(
          pair,
          sell_quantity,
          sell_price,
          { recvWindow: 10000000 }
        );
        this.checkAssetBalanceAtEnd();
        console.log(sell_order);
        let sell_msg =
          "Place SELL order For " +
          sell_quantity +
          " " +
          asset +
          " With Price " +
          sell_price;
        chartData += '{"date" : "' + this.formatDate(sell_order.timestamp) + '", "value" : ' + sell_price + ', "customBullet" : "assets/svg/sell.svg"}';
        chartData += ',';
        this.addToStat_msg("SELL",pair,sell_quantity,sell_price,sell_order.timestamp);
        if (sell_order != null) {
          if (this.exc_s == "cryptopia" && sell_order["id"] == undefined) {
            console.log("Sell Filled");
            this.checkBalanceAtEnd();
            this.last_sell_id = undefined;
            this.isSellFilled = true;
            this.isSell = false;
          } else {
            this.last_sell_id = sell_order["id"];
            console.log(this.last_sell_id);
            this.isSellFilled = false;
            let order_state = await this.ccxt_current.fetchOrder(
              this.last_sell_id,
              pair,
              { recvWindow: 10000000 }
            );
            if (order_state["remaining"] == 0) {
              let count=0;
              let percent=0.0;
              percent =parseFloat(this.portfolio_mm_s)/100;
              if(this.fillled_order.length>0){
                $.each(this.fillled_order, (i, val1) => {
                  if (val1["id"] == order_state["id"]) {
                    count+=1;
                  }
                });
              }
              if(count==0){
                this.fillled_order.unshift(order_state);
                sellvol_data=order_state["filled"]*order_state["price"];
                this.sellvol+=sellvol_data;
                this.noof_tran+=1;
                sellvol_data=0;
                this.checkBalanceAtEnd();
              }
              this.last_sell_id = undefined;
              this.isSellFilled = true;
              this.isSell = false;
            }
          }
        }
      }
      // BUY
      if (this.isBuy) {
        console.log(buy_quantity, buy_price);
        let buy_order = await this.ccxt_current.create_limit_buy_order(
          pair,
          buy_quantity,
          buy_price,
          { recvWindow: 10000000 }
        );
        this.checkAssetBalanceAtEnd();
        chartData += '{"date" : "' + this.formatDate(buy_order.timestamp) + '", "value" : ' + buy_price + ', "customBullet" : "assets/svg/buy.svg"}';
        this.addToStat_msg("BUY",pair,buy_quantity,buy_price,buy_order.timestamp);
        if (buy_order != null) {
          if (this.exc_s == "cryptopia" && buy_order["id"] == undefined) {
            console.log("Buy Filled");
            this.last_buy_id = undefined;
            this.isBuyFilled = true;
            this.isBuy = false;
          } else {
            this.last_buy_id = buy_order["id"];
            console.log(this.last_buy_id);
            this.isBuyFilled = false;
            let order_state = await this.ccxt_current.fetchOrder(
              this.last_buy_id,
              pair,
              { recvWindow: 10000000 }
            );
            if (order_state["remaining"] == 0) {
              let count=0;
              let percent=0.0;
              percent = parseFloat(this.portfolio_mm_s)/100;
              if(this.fillled_order.length>0){
                $.each(this.fillled_order, (i, val1) => {
                  if (val1["id"] == order_state["id"]) {
                    count+=1;
                  }
                });
              }
              if(count==0){
                this.fillled_order.unshift(order_state);
                buyvol_data=order_state["filled"]*order_state["price"];
                this.buyvol+=buyvol_data;
                this.noof_tran+=1;
                this.checkBalanceAtEnd();
                buyvol_data=0;

              }
              this.last_buy_id = undefined;
              this.isBuyFilled = true;
              this.isBuy = false;
            }
          }
        }
      }

      if (this.isBuyFilled && this.isSellFilled) {
        this.isBuy = true;
        this.isSell = true;
        this.base_balance = base_bal;
        this.asset_balance = asset_bal;
        if (this.select_portfolio_type == "random") {
          this.portfolio_mm_s = this.randomIntFromInterval(
            this.portfolio_mm_s_from,
            this.portfolio_mm_s_to
          );
          await this.updateAssetBalance();
          this.base_balance = this.amount_to_use;
          this.asset_balance = this.amount_to_use;
        }
      }
    }
  } catch (e) {
    this.electron_er(e);
    this.StopMarketMaker();
  }
    chartData += ']';
    console.log(chartData);
    this.chartData = (JSON.parse(chartData));
    console.log(this.chartData);
    $('#chart').css('height', '100%');
    let chart_config = {
      "type": "serial",
      "theme": "dark",
      "addClassNames": true,
      "dataProvider": this.chartData,
      "dataDateFormat": "YYYY-MM-DD HH:NN",
      "categoryField": "date",
      "creditsPosition": "bottom-right",
      "valueAxes": [{
        "axisAlpha": 0.2,
        "dashLength": 1,
        "position": "left"
      }],
      "defs": {
        "filter": {
          "id": "dropshadow",
          "x": "-10%",
          "y": "-10%",
          "width": "120%",
          "height": "120%",
          "feOffset": {
            "result": "offOut",
            "in": "rgba",
            "dx": "3",
            "dy": "3"
          },
          "feGaussianBlur": {
            "result": "blurOut",
            "in": "offOut",
            "stdDeviation": "5"
          },
          "feBlend": {
            "in": "SourceGraphic",
            "in2": "blurOut",
            "mode": "normal"
          }
        }
      },
      "graphs": [{
        "id": "g1",
        "type": "smoothedLine",
        "bulletSize": 28,
        // "customBullet": '',
        "customBulletField": "customBullet",
        "balloonText": "[[value]]",
        "lineColor": "var(--blue-2)",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 0,
        "valueField": "value",
        "useLineColorForBulletBorder": true,
        "lineThickness": 2
        // "balloon":{
        //     "drop":true
        // }
      }],
      "chartCursor": {
        "categoryBalloonDateFormat": "HH:NN, DD MMMM",
        "cursorPosition": "mouse"
      },
      "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
      },
      "categoryAxis": {
        "minPeriod": "mm",
        "parseDates": true,
        "axisColor": "#DADADA",
        "dashLength": 1,
        "minorGridEnabled": true
      }
    };

    this.chart = this.AmCharts.makeChart("chart", chart_config);
    $('.chart_preloader').fadeOut(200);
}
  checkBalanceAtEnd() {
    var scope=this;
    (async () => {
      let fetchBalance = await this.ccxt_current.fetchBalance({
        recvWindow: 10000000
      });
      let fetchFreeBalance = fetchBalance.free;
      if (!(this.base in fetchFreeBalance)||!(this.asset in fetchFreeBalance)) {
        this.electron_er("couldnot find in your wallet");
        return;
      } else {
        $.each(fetchBalance.free, (i, val) => {
          if (i == this.base) {
            scope.end_maker_bal_base=val;
          }
          else if(i == this.asset){
            scope.end_maker_bal_asset=val;
          }
        });
      }
    })();
}
  StopMarketMaker() {
  this.checkInterval=1;
  clearInterval(this.marketmakerInterval);
  $('.page-body #marketmaker .overlay').fadeOut(200);
  if(this.fillled_order.length>0){
    let d = new Date();
    this.end_date=d.getTime();
    let endbal=this.end_maker_bal;
    let mak_dat=JSON.stringify(this.form_dat);
    let stat_msg=JSON.stringify(this.fillled_order);
    let beg_bal=this.start_maker_bal;
    let enddat=this.end_date;
    let beg_dat=this.strt_date;
    let no_of_tran=this.noof_tran;
    let pl_status=parseFloat(endbal)-parseFloat(beg_bal);
    let total_trade_vol=this.buyvol+this.sellvol;
    let mm_pl_status=this.sellvol-this.buyvol;
    let uname=JSON.parse(localStorage.autonio_login_token).username;
    let endbal_asset=this.end_maker_bal_asset;
    let endbal_base=this.end_maker_bal_base;
    let begbal_asset=this.start_maker_bal_asset;
    let begbal_base=this.start_maker_bal_base;
    let data={
      'uname': uname,
      'market_data':mak_dat,
      'end_bal':endbal,
      'order_stat':stat_msg,
      'start_bal':beg_bal,
      'start_date':beg_dat,
      'stop_date':enddat,
      'no_of_transaction':no_of_tran,
      'profit_loss':pl_status,
      'total_trading_vol':total_trade_vol,
      'mm_profit_loss':mm_pl_status,
      'start_bal_asset':begbal_asset,
      'start_bal_base':begbal_base,
      'end_bal_asset':endbal_asset,
      'end_bal_base':endbal_base
    };
     $.ajax({
       method: 'POST',
       url: 'https://auton.io/metamask_login/save_market_maker_data.php',
       data: data,
       error: (data) => {
         this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
       },
       success: (data) => {
         data = parseInt(data);
         switch (data) {
           case 400:
             this.electron_er("Market Log saved succesfully");
             this.fillled_order=[];
             this.buyvol=0;
             this.sellvol=0;
             this.noof_tran=0;
             break;
           case 404:
             this.electron_er('Something Wrong happened, Please try again later');
             break;
         }
       }
     });
    }
  }
  ngOnDestroy(){
     if(this.live_interval){
       clearInterval(this.live_interval);
     }
   }
}
