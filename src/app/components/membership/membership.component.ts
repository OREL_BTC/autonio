import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})
export class MembershipComponent implements OnInit {
  autonio_token;
  nio_amnt: number;
  nio_address = '';

  constructor(private router: Router) { }

  electron_er(msg) {
    alert(msg);
  }
  //trigger from nginit to check access status of platform
checkAccess(uname){
  $.ajax({
    method: 'POST',
    url: 'https://autonio.foundation/metamask_login/get_access_permision.php',
  //  url: 'https://auton.io/webservices/user_check.php',
    data: { 'uname': uname },
    error: (data) => {
      this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
    },
    success: (data) => {
      this.nio_amnt=parseInt(data);
      // console.log("permission from backend==",this.nio_amt,data);
    }
  });
}

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
      let uname = JSON.parse(localStorage.autonio_login_token).username;
      this.checkAccess(uname);
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }

    // console.log(this.autonio_token);
    // if (this.autonio_token != null) {
    //   // console.log(this.autonio_token);
    //   let uname = this.autonio_token.username;
    //   $.ajax({
    //     url: 'https://auton.io/webservices/user_membership.php',
    //     method: 'POST',
    //     data: { 'username': uname },
    //     success: (data) => {
    //       data = parseInt(data);
    //       switch (data) {
    //         case 400:
    //           this.router.navigateByUrl('/live');
    //           break;
    //
    //         case 202:
    //           $.ajax({
    //             url: 'https://min-api.cryptocompare.com/data/histoday?fsym=NIO*&tsym=USD&limit=2&aggregate=1&e=CCCAGG',
    //             datatype: 'json',
    //             success: (data) => {
    //               // console.log(data);
    //               let Data = data['Data'];
    //               let close = Data[Data.length - 2]['close'];
    //               // console.log(close);
    //               this.nio_amnt = Math.round((50 / close));
    //               // console.log(this.nio_amnt);
    //             }
    //           });
    //           $.ajax({
    //             url: 'https://auton.io/webservices/get_address.php',
    //             data: { 'username': uname },
    //             method: 'POST',
    //             success: (data) => {
    //               this.nio_address = data;
    //             }
    //           });
    //           break;
    //
    //         case 404:
    //           this.electron_er('Some Error');
    //           break;
    //
    //       }
    //     }
    //   });
    // }
    // else {
    //   this.router.navigateByUrl('/login');
    // }

  }

  checkPayment(uname) {
    $.ajax({

      //url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address=' + this.nio_address + '&tag=latest&apikey=6XTEJQMYAPS3D2RXSZDRC6FXQ1MXDSDYC5',
      url: 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&address='+ uname+'&tag=latest&apikey=YourApiKeyToken',
      success: (data) => {
        let received_amnt = parseInt(data.result);
       //console.log("Nio token in your eth address is=",received_amnt);
       if(received_amnt>=this.nio_amnt){
         this.router.navigateByUrl('/live');
       }
      // success: (data) => {
      //   let received_amnt = parseInt(data.result);
      //   // console.log(received_amnt);
      //   let check_amount = this.nio_amnt * 0.8;
      //   console.log(check_amount);

        // if (received_amnt >= check_amount) {
        //   // console.log('Success');
        //   $.ajax({
        //     url: 'https://auton.io/webservices/user_upgrade.php',
        //     method: 'POST',
        //     data: { 'username': this.autonio_token.username, 'address': this.nio_address },
        //     success: (data1) => {
        //       data1 = parseInt(data1);
        //       console.log(data1);
        //       switch (data1) {
        //         case 400:
        //           // console.log('Success');
        //           this.router.navigateByUrl('/live');
        //           break;
        //
        //         case 404:
        //           this.electron_er('Some Error Occured, Please try again later');
        //           break;
        //       }
        //     }
        //   })
        // }
        // else {
        //   this.electron_er("Amount Don't Match, Please check you have send NIO amount to right address.");
        // }
      }
    });
  }
  logout() {
    localStorage.clear();
    console.log(localStorage.getItem('autonio_login_token'));

    this.router.navigateByUrl('/login');
  }
}
