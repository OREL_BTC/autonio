import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingSettingsComponent } from './selling-settings.component';

describe('SellingSettingsComponent', () => {
  let component: SellingSettingsComponent;
  let fixture: ComponentFixture<SellingSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
