; (function (h, b) {
    var g = function (a, b, d) {
        var c;
        return function () {
            var e = this
                , f = arguments;
            c ? clearTimeout(c) : d && a.apply(e, f);
            c = setTimeout(function () {
                d || a.apply(e, f);
                c = null
            }, b || 100)
        }
    };
    $.fn[b] = function (a) {
        return a ? this.bind("resize", g(a)) : this.trigger(b)
    }
})($, "smartresize");
function rockthemes_ae_steps() {
    rockthemes_ae_steps_init_resize();
    $(window).smartresize(rockthemes_ae_steps_init_resize);
    $('.azoom-steps .step-icon').click(function () {
        rockthemes_se_elem_clicked($(this));
    });
    $('.azoom-steps').each(function () {
        var that = $(this);
        var size = (parseInt(that.attr('data-min-width')) / that.find('li').length);
        var starting_x = 0;
        var ending_x = 0;
        var matrix = (that.css("transform")).substr(7, (that.css("transform")).length - 8).split(', ');
        var last_size = matrix[4];
        var first_x_val = -(parseInt(that.find('li').first().offset().left + ((parseInt(that.attr('data-min-width')) / that.find('li').length) / 2)) - 35);
        first_x_val = first_x_val + Number(last_size);
        var first_x_val_desk = -(parseInt(that.find('li').first().position().left + ((parseInt(that.attr('data-min-width')) / that.find('li').length) / 2)) - 35);
        first_x_val_desk = first_x_val_desk + Number(last_size);
        that.on('touchstart', function (e) {
            starting_x = e.originalEvent.touches[0].pageX;
            ending_x = parseInt(starting_x - e.originalEvent.touches[0].pageX);
            matrix = (that.css("transform")).substr(7, (that.css("transform")).length - 8).split(', ');
            last_size = matrix[4];
            that.removeClass('azoom-transition');
        }).on("touchmove", function (e) {
            e.preventDefault();
            ending_x = parseInt(starting_x - e.originalEvent.touches[0].pageX);
            var diff = parseInt(last_size - ending_x);
            if (Math.abs(ending_x) < 5)
                return;
            that.css({
                'transform': 'translateX(' + diff + 'px)',
                '-webkit-transform': 'translateX(' + diff + 'px)'
            });
        }).on("touchend", function (e) {
            that.addClass('azoom-transition');
            if (Math.abs(ending_x) < 5)
                return;
            var diff = parseInt(last_size - ending_x);
            var new_val = parseInt(last_size) - (ending_x + (size - ((ending_x) % size)));
            if (ending_x < 0) {
                new_val = parseInt(new_val) + (size * 2);
            }
            if (new_val < -(parseInt(that.attr('data-min-width')) - (size / 2))) {
                new_val = first_x_val;
            } else if (new_val > (first_x_val - 10)) {
                new_val = first_x_val;
            }
            that.css({
                'transform': 'translateX(' + new_val + 'px)',
                '-webkit-transform': 'translateX(' + new_val + 'px)'
            });
        });
        that.on('mousedown', function (e) {
            that.addClass('mousedown');
            starting_x = e.pageX - that.offset().left;
            ending_x = parseInt(starting_x - (e.pageX - that.offset().left));
            matrix = (that.css("transform")).substr(7, (that.css("transform")).length - 8).split(', ');
            last_size = matrix[4];
            that.removeClass('azoom-transition');
        });
        $('body').on("mousemove", function (e) {
            if (!that.hasClass('mousedown')) {
                e.preventDefault();
                return true;
            }
            e.stopPropagation();
            ending_x = parseInt(starting_x - (e.pageX - that.offset().left));
            var diff = parseInt(last_size - ending_x);
            if (Math.abs(ending_x) < 5)
                return;
            that.css({
                'transform': 'translateX(' + diff + 'px)',
                '-webkit-transform': 'translateX(' + diff + 'px)'
            });
        });
        $('body').on("mouseup", function (e) {
            e.preventDefault();
            if (!that.hasClass('mousedown'))
                return;
            that.removeClass('mousedown');
            that.addClass('azoom-transition');
            if (Math.abs(ending_x) < 5)
                return;
            var diff = parseInt(last_size - ending_x);
            var new_val = parseInt(last_size) - (ending_x + (size - ((ending_x) % size)));
            if (ending_x < 0) {
                new_val = parseInt(new_val) + (size * 2);
            }
            if (new_val < -(parseInt(that.attr('data-min-width')) - (size / 2))) {
                new_val = first_x_val_desk;
            } else if (new_val > (first_x_val_desk - 10)) {
                new_val = first_x_val_desk;
            }
            that.css({
                'transform': 'translateX(' + new_val + 'px)',
                '-webkit-transform': 'translateX(' + new_val + 'px)'
            });
        });
    });
    $('.azoom-steps .step-back').click(function (e) {
        var t_steps = $(this).parents('.azoom-steps');
        if (t_steps.parent().width() < parseInt(t_steps.attr('data-min-width')) && t_steps.find('li.active').length > 1) {
            var size = t_steps.find('li').first().width() + 20;
            var matrix = (t_steps.css("transform")).substr(7, (t_steps.css("transform")).length - 8).split(', ');
            var last_size = matrix[4];
            t_steps.css({
                'transform': 'translateX(' + (parseInt(last_size) + size) + 'px)',
                '-webkit-transform': 'translateX(' + (parseInt(last_size) + size) + 'px)',
                '-ms-transform': 'translateX(' + (parseInt(last_size) + size) + 'px)',
            });
        }
        rockthemes_se_elem_clicked($(this).parents('li').find('.step-icon'));
        if ($(this).parents('li').prev().length && $(this).parents('li').prev().hasClass('done')) {
            $(this).parents('li').prev().removeClass('done');
        }
    });
    $('.azoom-steps .step-next').click(function (e) {
        var t_steps = $(this).parents('.azoom-steps');
        if (t_steps.parent().width() < parseInt(t_steps.attr('data-min-width')) && t_steps.find('li.active').length < t_steps.find('li').length) {
            var size = t_steps.find('li').first().width() + 20;
            var matrix = (t_steps.css("transform")).substr(7, (t_steps.css("transform")).length - 8).split(', ');
            var last_size = matrix[4];
            t_steps.css({
                'transform': 'translateX(' + (parseInt(last_size) - size) + 'px)',
                '-webkit-transform': 'translateX(' + (parseInt(last_size) - size) + 'px)',
                '-ms-transform': 'translateX(' + (parseInt(last_size) - size) + 'px)',
            });
        }
        if ($(this).parents('li').next().length) {
            rockthemes_se_elem_clicked($(this).parents('li').next().find('.step-icon'));
        }
    });
    $('.azoom-steps').each(function () {
        var that = $(this);
        if (that.attr('data-start_steps') === 'none')
            return;
        that.appear();
        that.on('appear', function () {
            if (!that.find('.active').length) {
                if (!Modernizr.touch && that.attr('data-start_steps') === 'all' && parseInt(that.attr('data-min-width')) < $(window).width()) {
                    rockthemes_se_elem_clicked(that.find('li .step-icon'));
                } else {
                    rockthemes_se_elem_clicked(that.find('li').first().find('.step-icon'));
                }
                that.off('appear');
                that.appearOff();
            }
        });
    });
}
function rockthemes_ae_steps_init_resize() {
    $('.azoom-steps').each(function () {
        var data_width = typeof $(this).attr('data-min-width') !== '' && $(this).attr('data-min-width') !== '' ? parseInt($(this).attr('data-min-width')) : 960;
        if (parseInt($(this).find(' > ul > li').length % 2) === 0) {
            data_width += $(this).find(' > ul > li').first().width();
        }
        if ($(this).parent().width() < parseInt(data_width)) {
            var f = $(this).find('li').first();
            var x_val = -(parseInt(((parseInt($(this).attr('data-min-width')) / $(this).find('li').length) / 2)) - 35);
            if ($(this).hasClass('responsive')) {
                var matrix = ($(this).css("transform")).substr(7, ($(this).css("transform")).length - 8).split(', ');
                var last_size = matrix[4];
            }
            $(this).addClass('responsive');
            $(this).css({
                'min-width': $(this).attr('data-min-width') + 'px',
                'transform': 'translateX(' + x_val + 'px)',
                '-webkit-transform': 'translateX(' + x_val + 'px)',
                '-moz-transform': 'translateX(' + x_val + 'px)',
                '-ms-transform': 'translateX(' + x_val + 'px)'
            });
        } else if ($(this).hasClass('responsive')) {
            $(this).removeClass('responsive');
            $(this).css({
                'transform': 'translateX(0px)',
                '-webkit-transform': 'translateX(0px)',
                '-moz-transform': 'translateX(0px)',
                '-ms-transform': 'translateX(0px)'
            });
        }
    });
}
function rockthemes_se_elem_clicked(elem) {
    var that = elem.parents('li');
    var steps = that.parents('.azoom-steps');
    var start_class = steps.attr('class');
    if (steps.hasClass('connect-steps')) {
        var this_i = that.parents('ul').find('li').index(that);
        var not_connected = false;
        for (var b = 0; b < this_i; b++) {
            if (!steps.find('li:eq(' + b + ')').hasClass('active')) {
                not_connected = true;
                break;
            }
        }
        for (var a = this_i + 1; a < steps.find('li').length; a++) {
            if (steps.find('li:eq(' + a + ')').hasClass('active')) {
                not_connected = true;
                break;
            }
        }
        if (not_connected) {
            return;
        }
    }
    that.toggleClass('active');
    if (that.hasClass('done')) {
        that.removeClass('done');
    }
    if (that.hasClass('active')) {
        elem.css({
            'background-color': that.attr('data-step-color')
        });
        that.css({
            'background-color': that.attr('data-step-color')
        });
        that.find('.step-details-line').css({
            'background-color': that.attr('data-step-color')
        });
        if (that.index() % 2 === 0) {
            that.parents('.azoom-steps').addClass('azoom-steps-margin-bottom');
        } else {
            that.parents('.azoom-steps').addClass('azoom-steps-margin-top');
        }
    } else {
        elem.css({
            'background-color': ''
        });
        that.css({
            'background-color': ''
        });
        that.find('.step-details-line').css({
            'background-color': ''
        });
    }
    if (elem.parents('.azoom-steps').hasClass('jump-steps')) {
        if (that.hasClass('active')) {
            for (var i = 0; i < that.parents('ul').find('li').index(that); i++) {
                steps.find('li:eq(' + i + ')').find('.step-icon').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').find('.step-details-line').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').addClass('active done');
                if (i % 2 === 0) {
                    steps.addClass('azoom-steps-margin-bottom');
                } else {
                    steps.addClass('azoom-steps-margin-top');
                }
            }
        } else {
            for (var t = that.parents('ul').find('li').index(that); t < steps.find('li').length; t++) {
                steps.find('li:eq(' + t + ')').removeClass('active done');
                steps.find('li:eq(' + t + ')').find('.step-icon').css({
                    'background-color': ''
                });
                steps.find('li:eq(' + t + ')').css({
                    'background-color': ''
                });
                steps.find('li:eq(' + t + ')').find('.step-details-line').css({
                    'background-color': ''
                });
            }
        }
    } else {
        if (that.prev().length) {
            that.prev().addClass('done');
        }
    }
    if (!that.parents('.azoom-steps').find('.active').length) {
        that.parents('.azoom-steps').removeClass('azoom-steps-margin-top azoom-steps-margin-bottom');
    }
}
function rockthemes_se_elem_clicked(elem) {
    var that = elem.parents('li');
    var steps = that.parents('.azoom-steps');
    var start_class = steps.attr('class');
    if (steps.hasClass('connect-steps')) {
        var this_i = that.parents('ul').find('li').index(that);
        var not_connected = false;
        for (var b = 0; b < this_i; b++) {
            if (!steps.find('li:eq(' + b + ')').hasClass('active')) {
                not_connected = true;
                break;
            }
        }
        for (var a = this_i + 1; a < steps.find('li').length; a++) {
            if (steps.find('li:eq(' + a + ')').hasClass('active')) {
                not_connected = true;
                break;
            }
        }
        if (not_connected) {
            return;
        }
    }
    that.toggleClass('active');
    if (that.hasClass('done')) {
        that.removeClass('done');
    }
    if (that.hasClass('active')) {
        elem.css({
            'background-color': that.attr('data-step-color')
        });
        that.css({
            'background-color': that.attr('data-step-color')
        });
        that.find('.step-details-line').css({
            'background-color': that.attr('data-step-color')
        });
        if (that.index() % 2 === 0) {
            that.parents('.azoom-steps').addClass('azoom-steps-margin-bottom');
        } else {
            that.parents('.azoom-steps').addClass('azoom-steps-margin-top');
        }
    } else {
        elem.css({
            'background-color': ''
        });
        that.css({
            'background-color': ''
        });
        that.find('.step-details-line').css({
            'background-color': ''
        });
    }
    if (elem.parents('.azoom-steps').hasClass('jump-steps')) {
        if (that.hasClass('active')) {
            for (var i = 0; i < that.parents('ul').find('li').index(that); i++) {
                steps.find('li:eq(' + i + ')').find('.step-icon').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').find('.step-details-line').css({
                    'background-color': steps.find('li:eq(' + i + ')').attr('data-step-color')
                });
                steps.find('li:eq(' + i + ')').addClass('active done');
                if (i % 2 === 0) {
                    steps.addClass('azoom-steps-margin-bottom');
                } else {
                    steps.addClass('azoom-steps-margin-top');
                }
            }
        } else {
            for (var t = that.parents('ul').find('li').index(that); t < steps.find('li').length; t++) {
                steps.find('li:eq(' + t + ')').removeClass('active done');
                steps.find('li:eq(' + t + ')').find('.step-icon').css({
                    'background-color': ''
                });
                steps.find('li:eq(' + t + ')').css({
                    'background-color': ''
                });
                steps.find('li:eq(' + t + ')').find('.step-details-line').css({
                    'background-color': ''
                });
            }
        }
    } else {
        if (that.prev().length) {
            that.prev().addClass('done');
        }
    }
    if (!that.parents('.azoom-steps').find('.active').length) {
        that.parents('.azoom-steps').removeClass('azoom-steps-margin-top azoom-steps-margin-bottom');
    }
}

$(function () {
    var selectors = [];
    var check_binded = false;
    var check_lock = false;
    var defaults = {
        interval: 250,
        force_process: false
    }
    var $window = $(window);
    var $prior_appeared;
    function process() {
        if (selectors.length < 1)
            return;
        check_lock = false;
        var win = {
            width: $window.width(),
            height: $window.height(),
            left: $window.scrollLeft(),
            top: $window.scrollTop()
        }
        for (var index = 0; index < selectors.length; index++) {
            var $appeared = $(selectors[index]).filter(function () {
                var el = $(this);
                var offset = el.offset();
                var top = offset.top;
                var left = offset.left;
                if (top + el.height() >= win.top && top - (el.data('appear-top-offset') || 0) <= win.top + win.height && left + el.width() >= win.left && left - (el.data('appear-left-offset') || 0) <= win.left + win.width) {
                    return true;
                } else {
                    return false;
                }
                offset = null;
                top = null;
                el = null;
            });
            $appeared.trigger('appear', [$appeared]);
            if ($prior_appeared) {
                var $disappeared = $prior_appeared.not($appeared);
                $disappeared.trigger('disappear', [$disappeared]);
            }
            $prior_appeared = $appeared;
        }
        win = null;
    }
    $.expr[':']['appeared'] = function (element) {
        var $element = $(element);
        if (!$element.is(':visible')) {
            return false;
        }
        var window_left = $window.scrollLeft();
        var window_top = $window.scrollTop();
        var offset = $element.offset();
        var left = offset.left;
        var top = offset.top;
        if (top + $element.height() >= window_top && top - ($element.data('appear-top-offset') || 0) <= window_top + $window.height() && left + $element.width() >= window_left && left - ($element.data('appear-left-offset') || 0) <= window_left + $window.width()) {
            return true;
        } else {
            return false;
        }
    }
    $.fn.extend({
        appear: function (options) {
            var opts = $.extend({}, defaults, options || {});
            var selector = this.selector || this;
            selectors.push(selector);
            if (!check_binded) {
                var on_check = function () {
                    if (check_lock) {
                        return;
                    }
                    check_lock = true;
                    process();
                };
                check_binded = true;
            }
            process();
            return $(selector);
        }
    });
    $.fn.extend({
        appearOff: function () {
            var selector = this.selector || this;
            var exists_int = selectors.indexOf(selector);
            if (exists_int > 1) {
                selectors.splice(exists_int, 1);
            }
            return $(selector);
        }
    });
    var throt_func = process;
    $(window).load(function () {

        $(window).scroll(throt_func).resize(throt_func);
        setTimeout(throt_func, 400);
    });
    if ($('.azoom-steps').length) {
        rockthemes_ae_steps();
    }
});
